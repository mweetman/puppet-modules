# Class: networker
#
#  Manages the networker configuration, package and service
#
# Parameters:
#
#  [enabled]
#   Desc: Determine whether the ntpd service should be installed and running
#   Args: <boolean>
#   Default: false
#
#  [cron_command]
#   Desc: Command to execute in the Networker cron job
#   Args: <string>
#   Default:
#
#  [cron_environment]
#   Desc: Environment for the Networker cron job
#   Args: <string>
#   Default: 'PATH=/usr/sbin:/usr/bin:/sbin:/bin'
#
#  [cron_hour]
#   Desc: Time to execute the Networker cron job
#   Args: <string>
#   Default: 3
#
# Sample Usage:
#
#  class { 'networker': }
#
class networker(
  $enabled = false,
  $service_name = 'networker',
  $cron_command = '',
  $cron_environment = 'PATH=/usr/sbin:/usr/bin:/sbin:/bin',
  $cron_hour = 3)
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    $file_ensure    = 'file'
  }
  else {
    $package_ensure = 'absent'
    $service_ensure = 'stopped'
    $service_enable = false
    $file_ensure    = 'absent'
  }
  if $::is_virtual {
    $package_list = ['ksh','glibc.i686','nss-softokn-freebl.i686','lgtoclnt','lgtoman']
    $cron_ensure = 'absent'
  }
  else {
    $package_list = ['ksh','glibc.i686','nss-softokn-freebl.i686','lgtoclnt','lgtoman','nbmr']
    $cron_ensure = 'present'
  }
  # resource defaults
  # resources
  package { $package_list:
    ensure  =>  $package_ensure,
    before  =>  Service["$service_name"],
  }
  service { "$service_name":
    ensure      => $service_ensure,
    enable      => $service_enable,
    hasrestart  => true,
    hasstatus   => true,
  }
  cron { 'cristie_cron':
    command     => $cron_command,
    environment => $cron_environment,
    user        => undef,
    hour        => $cron_hour,
    ensure      => $cron_ensure,
  }
}

