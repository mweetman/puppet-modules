# Class: protocols
#
#  Disables various network protocols
#
# Parameters:
#
#  [<protocol>_disabled]
#   Desc: Whether the protocol is disabled
#   Args: <boolean>
#   Default: true
#
# Sample Usage:
#
#  class { 'protocols': ipv6_disabled => true, }
#
class protocols(
  $dccp_disabled = true,
  $ipv6_disabled = true,
  $rds_disabled  = true,
  $sctp_disabled = true,
  $tipc_disabled = true)
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  $dccp_file_ensure = $dccp_disabled ? {
        false          => 'absent',
        default        => 'present',
  }
  $ipv6_file_ensure = $ipv6_disabled ? {
        false          => 'absent',
        default        => 'present',
  }
  $rds_file_ensure = $rds_disabled ? {
        false          => 'absent',
        default        => 'present',
  }
  $sctp_file_ensure = $sctp_disabled ? {
        false          => 'absent',
        default        => 'present',
  }
  $tipc_file_ensure = $tipc_disabled ? {
        false          => 'absent',
        default        => 'present',
  }
  # Resource defaults
  File {
    group    => 'root',
    mode     => '0644',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'modules_conf_t',
    seluser  =>  undef,
  }
  # Resources
  file { '/etc/modprobe.d/dccp.conf':
    ensure   => $dccp_file_ensure,
    content  => "install dccp /bin/true\n",
  }
  file { '/etc/modprobe.d/ipv6.conf':
    ensure   => $ipv6_file_ensure,
    content  => "options ipv6 disable=1\n",
  }
  file { '/etc/modprobe.d/rds.conf':
    ensure   => $rds_file_ensure,
    content  => "install rds /bin/true\n",
  }
  file { '/etc/modprobe.d/sctp.conf':
    ensure   => $sctp_file_ensure,
    content  => "install sctp /bin/true\n",
  }
  file { '/etc/modprobe.d/tipc.conf':
    ensure   => $tipc_file_ensure,
    content  => "install tipc /bin/true\n",
  }
}
