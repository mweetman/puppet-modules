class { 'users':
         users => {
                  'morgs' => {
                             'comment' => 'Super Ninja',
                             'groups'  => ['wheel', 'root', 'ops'],
                             'home'    => '/opt/morgs',
                             'name'    => 'morgan',
                             'uid'     => 6666,
                             },
                  'devo'  => {
                             'comment' => 'The Jooru',
                             'groups'  => ['wheel', 'root', 'ops'],
                             'home'    => '/home/jooru',
                             'name'    => 'djoo',
                             },
                  },
         groups => {
                   'ops' => {
                              'name'    => 'ops',
                              'gid'     => 123,
                              'system'  => true,
                              },
                   'devs'  => {
                              'name'    => 'devs',
                              'system'  => false,
                              },
                   },
}
