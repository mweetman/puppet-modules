# Class: users
#
#  This module ensures local users are defined
#
# Parameters:
#
#  [users]
#   Desc: hash of user definitions and their properties
#   Args: <hash of hashes>
#   Default: empty
#
#  [groups]
#   Desc: hash of group definitions and their properties
#   Args: <hash of hashes>
#   Default: empty
#
# Sample Usage:
#
#  class { 'users': }
#
class users(
  $users = hiera_hash('users::users', {}),
  $groups = hiera_hash('users::groups', {}))
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  define manage_groups(
    $ensure = 'present',
    $gid = undef,
    $system = undef,
    $name)
    {
    group { "$name":
      ensure           => "$ensure",
      gid              => $gid,
      name             => "$name",
      system           => $system,
    }
  }
  define manage_users(
    $ensure = 'present',
    $comment = undef,
    $gid = undef,
    $groups = undef,
    $home = undef,
    $managehome = 'yes',
    $membership = 'minimum',
    $name,
    $password = undef,
    $shell = undef,
    $uid = undef)
    {
    user { "$name":
      comment          => $comment,
      ensure           => "$ensure",
      gid              => $gid,
      groups           => $groups,
      home             => $home,
      managehome       => "$managehome",
      membership       => "$membership",
      name             => "$name",
      password         => $password,
      shell            => $shell,
      uid              => $uid,
    }
  }
  create_resources('manage_groups', $groups)
  create_resources('manage_users', $users)
}
