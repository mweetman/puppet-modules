# Class: accts_nologin
#
#  This module ensures defined accounts shell are set to /sbin/nologin
#
# Parameters:
#
#  [accts]
#   Desc: System accounts to restrict
#   Args: <array>
#   Default: empty
#
# Sample Usage:
#
#  class { 'accts_nologin': }
#
class accts_nologin(
  $accts = [])
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  define nologin() {
    exec { "accts_nologin_${name}":
      path    =>  '/bin:/usr/bin:/sbin:/usr/sbin',
      command =>  "usermod -s /sbin/nologin ${name}",
      unless  =>  "grep ^${name}: /etc/passwd | cut -d: -f7 | grep nologin$",
    }
  }
  nologin{ $accts: }
}

