# Class: autofs
#
#  Enables/disables autofs
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service should be enabled
#   Args: <boolean>
#   Default: false
#
# Sample Usage:
#
#  class { 'autofs': enabled => false, }
#
class autofs(
  $enabled = false,
  $package_name = 'autofs',
  $service_name = 'autofs')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    Package["$package_name"] -> Service["$service_name"]
  }
  else {
    $package_ensure = 'absent'
    $service_ensure = 'stopped'
    $service_enable = false
    Service["$service_name"] -> Package["$package_name"]
  }
  package { "$package_name":
    ensure      =>  $package_ensure,
  }
  service { "$service_name":
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    hasrestart  =>  true,
    hasstatus   =>  true,
  }
}
