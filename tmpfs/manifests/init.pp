# Class: tmpfs
#
# Enables/disables tmp.mount
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service will be enabled
#   Args: <boolean>
#   Default: false
#
# Sample Usage:
#
#  class { tmpfs: enabled => true, }
#
class tmpfs(
  $enabled = false,
  $service_name = 'tmp.mount')
  {
  case $::osfamily {
    'RedHat': {
      case $::operatingsystemmajrelease {
        '6': {
          fail("The ${module_name} module is not supported on ${::osfamily} ${::operatingsystemmajrelease} based systems.")
        }
      }
    }
    default: {
      fail("The ${module_name} module is not supported on ${::osfamily} based systems.")
    }
  }
  if $enabled {
   $service_ensure = 'running'
   $service_enable = true
  }
  else {
    $service_ensure = 'stopped'
    $service_enable = false
  }
  service { "$service_name":
    ensure  => $service_ensure,
    enable  => $service_enable,
    hasrestart => true,
    hasstatus => true,
  }
}
