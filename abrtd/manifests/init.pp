# Class: abrtd
#
#  Manages the abrtd service
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the abrtd service should be enabled
#   Args: <boolean>
#   Default: false
#
# Requires:
#
# Sample Usage:
#
#  class { 'abrtd': enabled => false,}
#
class abrtd(
  $enabled = false,
  $package_name = 'abrt',
  $service_name = 'abrtd')
  {
  case $::osfamily {
    'RedHat': {
      case $::operatingsystemmajrelease {
        '6': {
          $service_name_dbus = 'messagebus'
        }
        default: {
          $service_name_dbus = 'dbus'
        }
      }
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    Service["$service_name_dbus"] -> Package["$package_name"]
    Package["$package_name"] -> Service["$service_name"]
  }
  else {
    $package_ensure = 'absent'
    $service_ensure = 'stopped'
    $service_enable = false
    Service["$service_name"] -> Package["$package_name"]
  }
  # resources
  package { "$package_name":
    ensure      =>  $package_ensure,
  }
  service { "$service_name":
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    require     =>  Service["$service_name_dbus"],
  }
}

