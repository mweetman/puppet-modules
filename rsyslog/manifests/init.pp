# Class: rsyslog
#
#  Enables/disables rsyslog
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service will be enabled
#   Args: <boolean>
#   Default: true
#
#  [siem_host]
#   Desc: Security Information and Event Management host
#   Args: <string>
#   Default: empty
#
# Sample Usage:
#
#  class { 'rsyslog': enabled => false, }
#
class rsyslog(
  $enabled =  true,
  $package_name = 'rsyslog',
  $service_name = 'rsyslog',
  $siem_host = '')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure   = 'present'
    $service_ensure   = 'running'
    $service_enable   = true
    $file_ensure      = 'file'
    $directory_ensure = 'directory'
    Package["$package_name"] -> File['/etc/rsyslog.conf'] -> Service["$service_name"]
    Package["$package_name"] -> File['/etc/rsyslog.d'] -> Service["$service_name"]
    Package["$package_name"] -> File['/etc/rsyslog.d/siem.conf'] -> Service["$service_name"]
  }
  else {
    $package_ensure   = 'absent'
    $service_ensure   = 'stopped'
    $service_enable   = false
    $file_ensure      = 'absent'
    $directory_ensure = 'absent'
    Service["$service_name"] -> File['/etc/rsyslog.conf'] -> Package["$package_name"]
    Service["$service_name"] -> File['/etc/rsyslog.d'] -> Package["$package_name"]
    Service["$service_name"] -> File['/etc/rsyslog.d/siem.conf'] -> Package["$package_name"]
  }
  # resource defaults 
  File {
    group    => 'root',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'syslog_conf_t',
    seluser  => undef, 
  }
  package { "$package_name":
    ensure      =>  $package_ensure,
  }
  service { "$service_name":
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    hasrestart  =>  true,
    hasstatus   =>  true,
  }
  file { '/etc/rsyslog.conf':
    ensure   => $file_ensure,
    source   => "puppet:///modules/rsyslog/${::osfamily}/${::operatingsystemmajrelease}/rsyslog.conf",
    mode     => '0644',
  }
  file { '/etc/rsyslog.d/siem.conf':
    ensure   => 'file',
    content  => template('rsyslog/siem.conf.erb'),
    mode     => '0644',
  }
  file { '/etc/rsyslog.d':
    ensure   => $directory_ensure,
    mode     => '0755',
  }
  file { '/etc/logrotate.d/syslog':
    ensure   => 'file',
    source   => "puppet:///modules/rsyslog/logrotate/syslog",
    mode     => '0644',
    seltype  => 'etc_t',
  }
  file { '/var/log/messages':
    ensure   => 'file',
    mode     => '0600',
    seltype  => 'var_log_t',
  }
  file { '/var/log/secure':
    ensure   => 'file',
    mode     => '0600',
    seltype  => 'var_log_t',
  }
  file { '/var/log/maillog':
    ensure   => 'file',
    mode     => '0600',
    seltype  => 'var_log_t',
  }
  file { '/var/log/cron':
    ensure   => 'file',
    mode     => '0600',
    seltype  => 'cron_log_t',
  }
}
