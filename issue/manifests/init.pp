# Class: issue
#
#  This module manages /etc/issue and /etc/issue.net
#
# Sample Usage:
#
#  class { 'issue': 
#        content => "## Login Banner ##\nYour actions are being monitored\n"
#        }
#
class issue(
  $content = 
'*---------------------- NOTICE - PROPRIETARY SYSTEM -----------------------*
*    Access to this computer system is limited to authorised users only.   *
*    Unauthorised users may be subject to prosecution under the Crimes     *
*                         Act or State legislation.                        *
*                                                                          *
*       Please note, ALL CUSTOMER DETAILS are confidential and must        *
*                              not be disclosed.                           *
*--------------------------------------------------------------------------*

' )
  {
  file { '/etc/issue':
    ensure   => 'file',
    content  => "$content",
    group    => 'root',
    mode     => '0644',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'etc_runtime_t',
    seluser  => undef, 
  }
  file { '/etc/issue.net':
    ensure   => 'link',
    target   => '/etc/issue',
  }
}
