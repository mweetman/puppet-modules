# Class: rhsmcertd
#
#  Enables/disables rhsmcertd
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service will be enabled
#   Args: <boolean>
#   Default: false
#
# Sample Usage:
#
#  class { 'rhsmcertd': enabled => false, }
#
class rhsmcertd(
  $enabled = false,
  $service_name = 'rhsmcertd')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $service_ensure = 'running'
    $service_enable = true
  }
  else {
    $service_ensure = 'stopped'
    $service_enable = false
  }
  service { "$service_name":
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    hasrestart  =>  true,
    # status is broken, fixed in subscription-manager-1.0.3
    hasstatus   =>  false,
    status      =>  'pgrep rhsmcertd',
  }
}
