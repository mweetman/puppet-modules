class { 'chrony':
         enabled => true,
         servers => ['ntp1.lab.internal','ntp2.lab.internal'],
         clients => {'10.0.0.0' => '24', '192.168.0.0' => '16'},
}
