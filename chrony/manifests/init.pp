# Class: chrony
#
#  Manages the chrony configuration, package and service
#
# Parameters:
#
#  [enabled]
#   Desc: Determine whether the chrony service should be installed and running
#   Args: <boolean>
#   Default: true
#
#  [servers]
#   Desc: time servers to use
#   Args: <array>
#   Default: empty
#
#  [clients]
#   Desc: clients to serve - network/mask pairs
#   Args: <hash>
#   Default: empty
#
# Sample Usage:
#
#  class { 'chrony': enabled => false, }
#
class chrony(
  $enabled = true,
  $package_name = 'chrony',
  $service_name = 'chronyd',
  $servers = [],
  $clients = {})
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    $file_ensure    = 'file'
    Package["$package_name"] -> File['/etc/chrony.conf']
    File['/etc/chrony.conf'] ~> Service["$service_name"]
  }
  else {
    $package_ensure = 'absent'
    $service_ensure = 'stopped'
    $service_enable = false
    $file_ensure    = 'absent'
    Service["$service_name"] -> File['/etc/chrony.conf']
    File['/etc/chrony.conf'] -> Package["$package_name"]
  }
  # resource defaults
  File {
    owner    => 'root',
    group    => 'root',
    mode     => '0644',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'etc_t',
    seluser  => undef, 
  }
  # resources
  package { "$package_name":
    ensure  =>  $package_ensure,
  }
  file { '/etc/chrony.conf':
    ensure   => $file_ensure,
    content  => template('chrony/chrony.conf.erb'),
  }
  service { "$service_name":
    ensure      => $service_ensure,
    enable      => $service_enable,
    hasrestart  => true,
    hasstatus   => true,
  }
}
