# Class: udev_rules
#
#  Manages udev_rules
#
# Sample Usage:
#
#  class { 'udev_rules': }
#
class udev_rules {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is not supported on ${::osfamily} based systems.")
    }
  }
  File['/etc/udev/rules.d/80-readonly-removables.rules'] ~> Exec['udevadm_trigger']
  file { '/etc/udev/rules.d/80-readonly-removables.rules':
    ensure   => 'file',
    source   => 'puppet:///modules/udev_rules/80-readonly-removables.rules',
    group    => 'root',
    mode     => '0644',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'udev_rules_t',
    seluser  => undef, 
  }
  exec { 'udevadm_trigger':
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    command     => "udevadm trigger",
    refreshonly => true,
  }
}
