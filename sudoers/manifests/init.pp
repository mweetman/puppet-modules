# Class: sudoers
#
#  Manages the content of /etc/sudoers and /etc/sudoers.d/
#
# Parameters:
#
#  [sudoersd_strict]
#   Desc: Whether non-managed files will be removed from /etc/sudoers.d/
#   Args: <boolean>
#   Default: true
#
# Sample Usage:
#
#  class { 'sudoers': }
#
class sudoers(
  $sudoersd_strict = true)
  {
  $sudoers_file = '/etc/sudoers'
  $temp_sudoers = '/etc/sudoers.pup'
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  # resource defaults
  File {
    ensure    =>  'file',
    owner     =>  'root',
    group     =>  'root',
    mode      =>  '0440',
    selrange  =>  's0',
    selrole   =>  'object_r',
    seltype   =>  'etc_t',
    seluser   =>  'system_u',
    require   =>  Package['sudo'],
  }
  Exec { 
    path      =>  '/usr/bin:/bin:/usr/sbin:/sbin',
    require   =>  Package['sudo'],
  }
  # resources
  package { 'sudo':
    ensure    =>  $package_ensure,
  }
  file { $temp_sudoers:
    source    =>  'puppet:///modules/sudoers/sudoers',
  }
  file { $sudoers_file:
    source    =>  $temp_sudoers,
    require   =>  Exec['validate_sudoers'],
  }
  file { '/etc/sudoers.d':
    ensure   => 'directory',
    group    => 'root',
    mode     => '0750',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'etc_t',
    seluser  => 'system_u',
    recurse  => $sudoersd_strict,
    purge    => $sudoersd_strict,
  }
  exec { 'validate_sudoers':
    command   =>  "visudo -c -q -f $temp_sudoers",
    unless    =>  "diff -q $temp_sudoers $sudoers_file",
    subscribe =>  File[$temp_sudoers],
    require   =>  File[$temp_sudoers],
  }
}

