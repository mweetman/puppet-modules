# Class: iptables
#
#  Enables/disables iptables and manages basic rules
#
# Hiera: true
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service will be enabled
#   Args: <boolean>
#   Default: true
#
#  [manage_rules]
#   Desc: Whether this module will manage iptables rules
#   Args: <boolean>
#   Default: false
#
#  [input_policy]
#   Desc: The policy for the INPUT chain
#   Args: DROP, ACCEPT
#   Default: DROP
#
#  [forward_policy]
#   Desc: The policy for the FORWARD chain
#   Args: DROP, ACCEPT
#   Default: DROP
#
#  [output_policy]
#   Desc: The policy for the OUTPUT chain
#   Args: DROP, ACCEPT
#   Default: ACCEPT
#
#  [exposed_services]
#   Desc: The services this host is providing
#   Args: hash of hashes - {'rulenum' => { 'service' => 'protocol'}}
#   Default: empty
#
#  [icmp]
#   Desc: Whether icmp traffic is permitted
#   Args: <boolean>
#   Default: true
#
# Sample Usage:
#
#  class { 'iptables':
#    enabled =>  true,
#    manage_rules  =>  true,
#    services  =>  {'3' => {'ssh' => 'tcp'}, '4' => {'domain' => 'udp'}, '5' => {'domain' => 'tcp'} },
#  }
#
class iptables(
  $enabled = true,
  $package_name = 'iptables',
  $service_name = 'iptables',
  $manage_rules = false,
  $input_policy = 'DROP',
  $forward_policy = 'DROP',
  $output_policy = 'ACCEPT',
  $icmp = true,
  $exposed_services = hiera_hash('iptables::exposed_services', {}))
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  Package["$package_name"] -> Service["$service_name"]
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    if $manage_rules {
      $base = '/root/iptables'
      $staging = "${base}/staging"
      File["${staging}/00-header"] ~> Exec['build_rules']
      File["${staging}/01-policies"] ~> Exec['build_rules']
      File["${staging}/02-established"] ~> Exec['build_rules']
      File["${staging}/03-loopback"] ~> Exec['build_rules']
      File["${staging}/07-footer"] ~> Exec['build_rules']
      Exec['build_rules'] -> File['/etc/sysconfig/iptables']
      File['/etc/sysconfig/iptables'] ~> Service['iptables']
      File {
        ensure  =>  'file',
        owner   =>  'root',
        group   =>  'root',
        mode    =>  '0600',
      }
      file { "${base}":
        ensure  =>  'directory',
        mode    =>  '0700',
      }
      file { "${staging}":
        ensure  =>  'directory',
        mode    =>  '0700',
      }
      file { "${staging}/00-header":
        content =>  "* filter\n",
      }
      file { "${staging}/01-policies":
        content  =>  template('iptables/policies.erb'),
      }
      file { "${staging}/02-established":
        content =>  "-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT\n",
      }
      file { "${staging}/03-loopback":
        content =>  "-A INPUT -i lo -j ACCEPT\n",
      }
      file { "${staging}/04-services":
        content =>  template('iptables/services.erb'),
        notify  =>  Exec['build_rules'],
      }
      if $icmp {
        file { "${staging}/05-icmp":
          source  =>  'puppet:///modules/iptables/icmp',
          notify  =>  Exec['build_rules'],
        }
      }
      if $input_policy == 'ACCEPT' {
        file { "${staging}/06-input_dropall":
          content =>  "-A INPUT -j REJECT --reject-with icmp-host-prohibited\n",
          notify  =>  Exec['build_rules'],
        }
      }
      file { "${staging}/07-footer":
        content =>  "COMMIT\n",
      }
      exec { 'build_rules':
        path        =>  '/bin:/usr/bin:/sbin:/usr/sbin',
        command     =>  "/bin/cat ${staging}/* > ${base}/iptables",
        refreshonly =>  true,
      }
      file { '/etc/sysconfig/iptables':
        source  =>  "${base}/iptables",
      }
    }
  }
  else {
    $package_ensure = undef
    $service_ensure = 'stopped'
    $service_enable = false
  }
  package { "$package_name":
    ensure      =>  $package_ensure,
  }
  service { "$service_name":
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    hasrestart  =>  true,
    hasstatus   =>  true,
  }
}

