class { 'iptables':
         enabled =>  true,
         manage_rules  =>  true,
         # exposed_services => {'rulenum' => { 'service' => 'protocol'}}
         exposed_services  =>  {'3' => {'ssh' => 'tcp'}, '4' => {'domain' => 'udp'}, '5' => {'domain' => 'tcp'} },
}
