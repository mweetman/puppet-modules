# Class: shadow
#
#  This module manages permissions on passwd, group and shadow files
#
# Sample Usage:
#
#  class { 'shadow': }
#
class shadow
  {
  # resource defaults
  File {
    ensure   => 'file',
    owner    => 'root',
    group    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seluser  => undef, 
  }
  # resources
  file { '/etc/passwd':
    mode     => '0644',
    seltype  => 'passwd_file_t',
  }
  file { '/etc/shadow':
    mode     => '0000',
    seltype  => 'shadow_t',
  }
  file { '/etc/group':
    mode     => '0644',
    seltype  => 'passwd_file_t',
  }
  file { '/etc/gshadow':
    mode     => '0000',
    seltype  => 'shadow_t',
  }
}
