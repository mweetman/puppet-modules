# Class: motd
#
#  This module manages /etc/motd
#
# Parameters:
#
#  [content]
#   Desc: The message to display
#   Args: <string>
#   Default: shown below
#
# Sample Usage:
#
#  class { 'motd': 
#        content => "## Login Banner ##\nYour actions are being monitored\n"
#        }
#
class motd(
  $content = 
'Today will not be known as Taco Tuesday. It will be known as freedom Friday, but still on a Tuesday!

')
  {
  file { '/etc/motd':
    ensure   => 'file',
    content  => "$content",
    group    => 'root',
    mode     => '0644',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'etc_t',
    seluser  => undef,
  }
}
