# Class: useradd_defs
#
#  This module manages /etc/default/useradd
#
# Parameters:
#
#  [home]
#   Desc: default location to create home directories
#   Args: <string>
#   Default: /home
#
#  [shell]
#   Desc: default shell for new users
#   Args: <string>
#   Default: /bin/bash
#
#  [inactive]
#   Desc: number of days for an account to be inactive before it's disabled
#   Args: <string>
#   Default: 30
#
# Sample Usage:
#
#  class { 'useradd_defs': }
#
class useradd_defs (
  $home = '/home',
  $shell = '/bin/bash',
  $inactive = '30')
  {
  file { '/etc/default/useradd':
    ensure   => 'file',
    content  => template('useradd_defs/useradd.erb'),
    group    => 'root',
    mode     => '0600',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'etc_t',
    seluser  => undef, 
  }
}

