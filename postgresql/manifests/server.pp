# Class: postgresql::server
#
#  Manages the postgresql server configuration, package and service
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the postgresql server should be installed and running
#   Args: <boolean>
#   Default: false
#
#  [pkg_version]
#   Desc: Package version - part of the package name
#   Args: <string>
#   Default: empty
#
#  [svc_version]
#   Desc: Service version - part of the service name
#   Args: <string>
#   Default: empty
#
# Sample Usage:
#
#  class { 'postgresql::server': enabled     => true,
#                                pkg_version => '93',
#                                svc_version => '9.3'}
#
class postgresql::server(
  $enabled = false,
  $client_ips = [],
  $pkg_version = '93',
  $svc_version = '9.3')
  {
  if $enabled {
    $file_ensure    = 'file'
    $package_ensure = 'present'
    $service_enable = true
    $service_ensure = 'running'
    Package["postgresql${pkg_version}-server"] -> File['pg_hba.conf']
    File['pg_hba.conf'] ~> Service["postgresql-${svc_version}"]
  }
  else {
    $file_ensure    = 'absent'
    $package_ensure = 'absent'
    $service_enable = false
    $service_ensure = 'stopped'
    Service["postgresql-${svc_version}"] -> File['pg_hba.conf']
    File['pg_hba.conf'] -> Package["postgresql${pkg_version}-server"]
  }
  # resource defaults
  File {
    owner    => 'postgres',
    group    => 'postgres',
    mode     => '0600',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'postgresql_db_t',
    seluser  => undef, 
  }
  # resources
  package { "postgresql${pkg_version}-server":
    ensure  =>  $package_ensure,
  }
  file { 'pg_hba.conf':
    ensure   => $file_ensure,
    content  => template('postgresql/pg_hba.conf.erb'),
    path     => "/var/lib/pgsql/${svc_version}/data/pg_hba.conf",
  }
  service { "postgresql-${svc_version}":
    ensure      => $service_ensure,
    enable      => $service_enable,
    hasrestart  => true,
    hasstatus   => true,
  }
}
