# Class: postgresql::webconnect
#
#  Manages the postgresql connection requirements for web servers
#
# Parameters:
#
#  [enabled]
#   Desc: Determine whether the services should be installed and running
#   Args: <boolean>
#   Default: false
#
# Sample Usage:
#
#  class { 'postgresql::webconnect': enabled => true, }
#
class postgresql::webconnect(
  $enabled = false)
  {
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    $file_ensure    = 'file'
    Package['pgbouncer'] -> Service['pgbouncer']
  }
  else {
    $package_ensure = 'absent'
    $service_ensure = 'stopped'
    $service_enable = false
    $file_ensure    = 'absent'
    Service['pgbouncer'] -> Package['pgbouncer']
  }
  # resources
  package { 'pgbouncer':
    ensure  =>  $package_ensure,
  }
  package { 'python-psycopg2':
    ensure  =>  $package_ensure,
  }
  service { 'pgbouncer':
    ensure      => $service_ensure,
    enable      => $service_enable,
    hasrestart  => true,
    hasstatus   => true,
  }
}
