# Class: postgresql::client
#
#  Manages the postgresql package
#
# Parameters:
#
#  [enabled]
#   Desc: Determine whether the postgresql package should be installed
#   Args: <boolean>
#   Default: false
#
#  [pkg_version]
#   Desc: Package version - part of the package name
#   Args: <string>
#   Default: 93
#
# Sample Usage:
#
#  class { 'postgresql::client': enabled => true, pkg_version => '93' }
#
class postgresql::client(
  $enabled = false,
  $pkg_version = '93')
  {
  if $enabled {
    $package_ensure = 'present'
  }
  else {
    $package_ensure = 'absent'
  }
  # resources
  package { "postgresql${pkg_version}":
    ensure  =>  $package_ensure,
  }
}
