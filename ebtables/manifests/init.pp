# Class: ebtables
#
#  Enables/disables ebtables
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service will be enabled
#   Args: <boolean>
#   Default: false
#
# Sample Usage:
#
#  class { 'ebtables': enabled => true, }
#
class ebtables(
  $enabled = false,
  $package_name = 'ebtables',
  $service_name = 'ebtables')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is not supported on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $service_ensure = 'running'
    $service_enable = true
    $package_ensure = 'present'
    Package["$package_name"] -> Service["$service_name"]
  }
  else {
    $service_ensure = 'stopped'
    $service_enable = false
    $package_ensure = 'absent'
    Service["$service_name"] -> Package["$package_name"]
  }
  package { "$package_name":
    ensure      =>  $package_ensure,
  }
  service { "$service_name":
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    hasrestart  =>  true,
    hasstatus   =>  true,
  }
}
