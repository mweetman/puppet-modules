# Class: puppet
#
#  Manages the puppet client
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service will be enabled
#   Args: <boolean>
#   Default: true
#
#  [digest_algorithm]
#   Desc: The digest algorithm to use for hashing and file bucket
#   Args: <string>
#   Default: sha256
#
# Sample Usage:
#
#  class { 'puppet': enabled => true, }
#
class puppet(
  $enabled = true,
  $digest_algorithm = 'sha256')
  {
  case $::osfamily {
    'RedHat': {
      case $::operatingsystemmajrelease {
        '6': {
          $yum_path = '/usr/lib/ruby/site_ruby/1.8/puppet/provider/package/yum.rb'
          $sysconfig_logging_key = 'PUPPET_LOG'
          $sysconfig_logging_value = '/var/log/puppet/puppet.log'
        }
        default: {
          $yum_path = '/usr/share/ruby/vendor_ruby/puppet/provider/package/yum.rb'
          $sysconfig_logging_key = 'PUPPET_EXTRA_OPTS'
          $sysconfig_logging_value = '-l /var/log/puppet/puppet.log'
        }
      }
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    $file_ensure    = 'file'
    Package['puppet'] -> Service['puppet']
    Package['puppet'] -> File['yum_provider']
  }
  else {
    $package_ensure = 'absent'
    $service_ensure = 'stopped'
    $service_enable = false
    $file_ensure    = 'absent'
    Service['puppet'] -> Package['puppet']
    File['yum_provider'] -> Package['puppet']
  }
  package { 'puppet':
    ensure      =>  $package_ensure,
  }
  service { 'puppet':
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    hasrestart  =>  true,
    hasstatus   =>  true,
  }
  # This part is pretty ugly, you should probably avoid this module altogether
  # This resource modifies the yum provider to include an uninstall function,
  # which overrides rpm.uninstall(). This will remove dependencies when using
  # ensure => 'absent' which the standard yum provider can't do.
  # We assume that you're testing your modules in a dev environment first yes?
  file { 'yum_provider':
    path      =>  $yum_path,
    ensure    =>  $file_ensure,
    source    =>  "puppet:///modules/puppet/${::osfamily}/${::operatingsystemmajrelease}/yum.rb",
    group     =>  'root',
    mode      =>  '0644',
    owner     =>  'root',
    selrange  =>  's0',
    selrole   =>  'object_r',
    seluser   =>  'system_u',
  } 
  rhfuncs::insert_line { 'pluginsync':
    position   => '8',
    target     => '/etc/puppet/puppet.conf',
    content    => "pluginsync = true",
    before     => Rhfuncs::Insert_line['digest_algorithm'],
  }
  rhfuncs::insert_line { 'digest_algorithm':
    position   => '9',
    target     => '/etc/puppet/puppet.conf',
    content    => "digest_algorithm = $digest_algorithm",
  }
  rhfuncs::insert_key_value { "puppet_logging":
    target     => "/etc/sysconfig/puppet",
    key        => "$sysconfig_logging_key",
    value      => "$sysconfig_logging_value",
    spaces     => false,
    notify     => Service['puppet'],
  }
}
