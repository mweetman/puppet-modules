# Class: capabilities
#
#  This module disables linux capabilities system-wide using systemd
#
# Parameters:
#
#  [disable_ptrace]
#   Desc: disable the ability to ptrace processes
#   Args: <boolean>
#   Default: true
#
#  [disable_modules]
#   Desc: disable the ability to insert and remove modules after boot
#   Args: <boolean>
#   Default: true
#
# Sample Usage:
#
#  class { 'capabilities': }
#
class capabilities(
  $disable_ptrace = true,
  $disable_modules = true)
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  $ptrace = $disable_ptrace ? {
    true    => 'cap_sys_ptrace',
    default => '' 
  }
  $modules = $disable_modules ? {
    true    => 'cap_sys_module',
    default => '' 
  }
  if $disable_ptrace or
     $disable_modules {
    $value = "~$ptrace $modules"
  }
  else {
    $value = ""
  }
  # resources
  rhfuncs::insert_key_value { "systemd_system.conf":
    target     => "/etc/systemd/system.conf",
    key        => "CapabilityBoundingSet",
    value      => "$value",
    spaces     => false,
  }
}
