# Class: restorecond
#
#  Enables/disables restorecond
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service will be enabled
#   Args: <boolean>
#   Default: true
#
# Sample Usage:
#
#  class { 'restorecond': enabled => false, }
#
class restorecond(
  $enabled = true,
  $package_name = 'policycoreutils',
  $service_name = 'restorecond')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $service_ensure = 'running'
    $service_enable = true
    Package["$package_name"] -> Service["$service_name"]
  }
  else {
    $service_ensure = 'stopped'
    $service_enable = false
  }
  package { "$package_name":
    ensure      =>  'present',
  }
  service { "$service_name":
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    hasrestart  =>  true,
    hasstatus   =>  true,
  }
}
