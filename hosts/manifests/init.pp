# Class: hosts
#
#  Manages /etc/hosts
#
# Parameters:
#
#  [hosts_entries]
#   Desc: ip/hostname pairs
#   Args: <hash>
#   Default: empty
#
# Sample Usage:
#
#  class { 'hosts': }
#
class hosts(
  $hosts_entries = hiera_hash('hosts::entries', {}))
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  define add_hosts_entries($ip, $hostname) {
    rhfuncs::insert_key_value { "hosts_${ip}":
      target     => "/etc/hosts",
      key        => "$ip",
      value      => "$hostname",
      delimiter  => " ",
      spaces     => false,
    }
  }
  create_resources('add_hosts_entries', $hosts_entries)
}
