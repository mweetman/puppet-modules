These Puppet modules were created as a learning experience after Puppet Master training to replace a complex kickstart post section. They were originally designed to function with Hiera but have been updated to be compatible with Red Hat Satellite 6.

As you can see from the example Hiera config below, there is no intelligence in the modules regarding the supported OS major version/s - it is up to the ENC to provide this.

If you are developing Puppet modules there is also [a short Puppet module development guide](https://gitlab.com/mweetman/puppet-modules/blob/master/README_dev.md).

The following examples are for Hiera, there is Satellite information further down.

### Hiera
/etc/puppet/hiera.yaml:
```yaml
---
:backends:
  - json
:json:
  :datadir: /etc/puppet/hiera
:hierarchy:
  - "node/%{::fqdn}"
  - "roles/%{::role_web}"
  - "roles/%{::role_app}"
  - "roles/%{::role_db}"
  - "os/%{::osfamily}/%{::operatingsystemmajrelease}"
  - "os/%{::osfamily}"
  - common
```

/etc/puppet/hiera/common.json:
```json
{
   "-=comment_01=-": "Classes to include",
   "classes": [
    "abrtd",
    "abrt_ccpp",
    "abrt_oops",
    "accts_lock",
    "accts_nologin",
    "atd",
    "auditd",
    "authorized_keys",
    "chrony",
    "cpumgmt",
    "crond",
    "dbus",
    "device_drivers",
    "dnsmasq",
    "ebtables",
    "extra_facts",
    "filesystems",
    "firewalld",
    "gofer",
    "grub2",
    "init",
    "iptables",
    "irqbalance",
    "issue",
    "kdump",
    "limits",
    "login_defs",
    "lvm2_monitor",
    "lvm2_lvmetad",
    "mdmonitor",
    "mlocate",
    "motd",
    "ntp",
    "ntpdate",
    "oddjob",
    "pam_login",
    "password_quality",
    "postfix",
    "profiles",
    "protocols",
    "psacct",
    "puppet",
    "quota_nld",
    "rdisc",
    "restorecond",
    "rhfuncs",
    "rhnsd",
    "rhsmcertd",
    "rsyslog",
    "saslauthd",
    "scheduling",
    "securetty",
    "selinux",
    "shadow",
    "smartd",
    "snmp",
    "sshd",
    "su",
    "sudoers",
    "sysctl",
    "sysstat",
    "tcpwrappers",
    "tmpfs"
    "tuned",
    "udev_rules",
    "useradd_defs",
    "users",
    "xinetd"
    ],
   "-=comment_02=-": "Enable/disable services",
   "abrtd::enabled": true,
   "abrt_ccpp::enabled": false,
   "abrt_oops::enabled": false,
   "atd::enabled": false,
   "auditd::enabled": true,
   "chrony::enabled": true,
   "cpumgmt::enabled": true,
   "crond::enabled": true,
   "dbus::enabled": false,
   "dnsmasq::enabled": false,
   "ebtables::enabled": true,
   "firewalld::enabled": true,
   "gofer::enabled": true,
   "iptables::enabled": false,
   "irqbalance::enabled": "auto",
   "kdump::enabled": true,
   "lvm2_monitor::enabled": false,
   "mdmonitor::enabled": false,
   "mlocate::enabled": true,
   "ntp::enabled": false,
   "ntpdate::enabled": false,
   "oddjob::enabled": true,
   "postfix::enabled": true,
   "psacct::enabled": false,
   "puppet::enabled": true,
   "quota_nld::enabled": false,
   "rdisc::enabled": false,
   "rsyslog::enabled": true,
   "saslauthd::enabled": false,
   "smartd::enabled": false,
   "snmp::snmpd_enabled": false,
   "snmp::snmptrapd_enabled": false,
   "snmptrapd::enabled": false,
   "sshd::enabled": true,
   "sysstat::enabled": true,
   "tmpfs::enabled": false,
   "tuned::enabled": true,
   "xinetd::enabled": true,
   "-=comment_03=-": "Configuration for classes that require it",
   "accts_lock::accts": [ "bin",
                          "daemon",
                          "adm",
                          "lp",
                          "sync",
                          "shutdown",
                          "halt",
                          "mail",
                          "operator",
                          "games",
                          "ftp",
                          "nobody" ],
   "accts_nologin::accts": [ "bin",
                          "daemon",
                          "adm",
                          "lp",
                          "sync",
                          "shutdown",
                          "halt",
                          "mail",
                          "operator",
                          "games",
                          "ftp",
                          "nobody" ],
   "authorized_keys::authorized_keys": {
                     "root": { "target": "/root/.ssh/authorized_keys",
                              "owner": "root",
                              "group": "root",
                              "keys": [ "<key1>", "<key2>"] },
                     "admin": { "target": "/home/admin/.ssh/authorized_keys",
                              "owner": "admin",
                              "group": "admin",
                              "keys": [ "<key3>", "<key4>"] }
                     },
   "chrony::servers": [
           "ntp1.example.com",
           "ntp2.example.com"
          ],
   "chrony::clients": {
                   "10.0.0.0": "24"
                   },
   "device_drivers::usb_storage_disabled": false,
   "filesystems::udf_disabled": false,
   "firewalld::zone_create": [ "POC", "test" ],
   "firewalld::zone_set_default": "drop",
   "firewalld::zone_set_target": {
                          "POC": {"zone": "POC", "target": "DROP"},
                          "test": {"zone": "test", "target": "DROP"}
                          },
   "firewalld::zone_add_service": {
                          "001": {"zone": "POC", "service": "smtp"},
                          "002": {"zone": "POC", "service": "ntp"},
                          "003": {"zone": "test", "service": "ftp"}
                          },
   "firewalld::zone_add_port": {
                         "001": {"zone": "POC", "port": "8080/tcp"},
                         "002": {"zone": "test", "port": "9000-9001/tcp"}
                          },
   "grub2::grub2_password": "grub.pbkdf2.sha512.10000.507BF2BACF740F9DB9A27EF5FC28E84E9AEFFEE933D235AEF6C44045A2FA4E8E10DE27E63C6A32A887C6FE15EB9079703851D7EDE7FEA048333147F3C9C9945A.67EC20E2276F7017B531694BA1C4921CCC785D68590488BA1FD0A335D711F0C76534E36FABBF4406011779FE8BDE663619D4557951B75E12B11E739FC93BAC5E",
   "hosts::entries": {
                     "blackhole": { "ip": "10.10.10.1", "hostname": "blackhole.lab.internal"},
                     "luckydns": { "ip": "8.8.8.8", "hostname": "luckydns.google.com  luckydns"},
                     "son": { "ip": "6.6.6.6", "hostname": "son.of.thebeast.com"}
                     },
   "issue::content": "Placeholder login banner",
   "limits::nofile": 16384,
   "login_defs::login_defs_entries": {
                        "001": {"key": "PASS_MAX_DAYS", "value": 90},
                        "002": {"key": "PASS_MIN_DAYS", "value": 7},
                        "003": {"key": "PASS_MIN_LEN", "value": 14},
                        "004": {"key": "PASS_WARN_AGE", "value": 7},
                        "005": {"key": "ENCRYPT_METHOD", "value": "SHA512"}
                        },
   "motd::content": "Just what do you think you're doing, Dave?",
   "password_quality::remembered_passwords": 20,
   "password_quality::minlen": 12,
   "profiles::daemon_umask": "027",
   "profiles::user_umask": "077",
   "profiles::http_proxy": "http://proxy.example.com:8080",
   "protocols::sctp_disabled": false,
   "puppet::digest_algorithm": "md5",
   "rsyslog::siem_host": "192.168.0.1",
   "scheduling::at_allowed_users": [ "root", "admin" ],
   "scheduling::cron_allowed_users": [ "root", "admin" ],
   "securetty::allowed_ttys": [ "console", "tty1", "tty2" ],
   "selinux::state": "enforcing",
   "sshd::permit_root_login": "yes",
   "sshd::allowed_users": ["admin", "root"],
   "sshd::listen_addresses": "192.168.0.25",
   "sysctl::entries": {
                      "005": { "key": "fs.file-max", "value": 327679 },
                      "010": { "key": "fs.suid_dumpable", "value": 0 },
                      "015": { "key": "kernel.core_uses_pid", "value": 1 },
                      "020": { "key": "kernel.msgmax", "value": 65536 },
                      "025": { "key": "kernel.msgmnb", "value": 65536 },
                      "030": { "key": "kernel.randomize_va_space", "value": 2 },
                      "035": { "key": "kernel.sem", "value": "250 32000 100 128" },
                      "040": { "key": "kernel.shmmax", "value": 1052125184 },
                      "045": { "key": "kernel.sysrq", "value": 0 },
                      "050": { "key": "net.core.rmem_default", "value": 262144 },
                      "055": { "key": "net.core.rmem_max", "value": 16777216 },
                      "060": { "key": "net.core.wmem_default", "value": 262144 },
                      "065": { "key": "net.core.wmem_max", "value": 16777216 },
                      "070": { "key": "net.ipv4.conf.all.send_redirects", "value": 0 },
                      "075": { "key": "net.ipv4.conf.all.accept_source_route", "value": 0 },
                      "080": { "key": "net.ipv4.conf.all.accept_redirects", "value": 0 },
                      "085": { "key": "net.ipv4.conf.all.secure_redirects", "value": 0 },
                      "090": { "key": "net.ipv4.conf.all.log_martians", "value": 0 },
                      "095": { "key": "net.ipv4.conf.all.rp_filter", "value": 1 },
                      "100": { "key": "net.ipv4.conf.default.accept_redirects", "value": 0 },
                      "105": { "key": "net.ipv4.conf.default.accept_source_route", "value": 0 },
                      "110": { "key": "net.ipv4.conf.default.rp_filter", "value": 1 },
                      "115": { "key": "net.ipv4.conf.default.secure_redirects", "value": 0 },
                      "120": { "key": "net.ipv4.conf.default.send_redirects", "value": 0 },
                      "125": { "key": "net.ipv4.icmp_echo_ignore_broadcasts", "value": 1 },
                      "130": { "key": "net.ipv4.ip_forward", "value": 0 },
                      "135": { "key": "net.ipv4.ip_local_port_range", "value": "9000 65500" },
                      "140": { "key": "net.ipv4.tcp_syncookies", "value": 1 },
                      "145": { "key": "net.ipv4.tcp_rmem", "value": "4096 87380 16777216" },
                      "150": { "key": "net.ipv4.tcp_wmem", "value": "4096 65536 16777216" }
                      },
   "tcpwrappers::allowed_services": {
     "sshd": "ALL"
    },
   "tuned::profile": "throughput-performance",
   "useradd_defs::inactive": 40,
   "users::users": {
                   "morgs": {
                              "comment": "Super Ninja",
                              "groups": [ "wheel", "root", "ops" ],
                              "home": "/opt/morgs",
                              "name": "morgan",
                              "password": "<encrypted_password>",
                              "uid": 6666
                             },
                   "devo": {
                              "comment": "The Jooru",
                              "groups": [ "wheel", "root", "ops" ],
                              "home": "/home/jooru",
                              "name": "djoo"
                           }
                   },
   "users::groups": {
                    "ops": {
                              "name": "ops",
                              "gid": 123,
                              "system": true
                           },
                   "devs": {
                              "name": "devs",
                              "system": false
                           }
                   }
}
```

/etc/puppet/hiera/os/RedHat/6.json:
```json
{
   "-=comment_01=-": "Classes to include",
   "classes": [
                "haldaemon",
                "ip6tables",
                "netfs",
                "restorecond"
    ],
   "-=comment_02=-": "Service configuration",
   "accts_lock::accts": [ "uucp",
                          "gopher" ],
   "accts_nologin::accts": [ "uucp",
                          "gopher" ],
   "dbus::service_name": "messagebus",
   "haldaemon::enabled": false,
   "ip6tables::enabled": false,
   "netfs::enabled": true,
   "restorecond::enabled": false,
   "sysctl::entries": {
                      "605": { "key": "kernel.exec-shield", "value": 1 }
                      }
}
```

/etc/puppet/hiera/os/RedHat/7.json:
```json
{
   "-=comment_01=-": "Classes to include",
   "classes": [
    ],
   "-=comment_02=-": "Service configuration",
   "cpumgmt::package_name": "kernel-tools",
   "cpumgmt::service_name": "cpupower",
   "dbus::enabled": true,
   "tmpfs::enabled": true
}
```

/etc/puppet/hiera/node/somehost.domain.com.json:
```json
{
   "ntp::servers": [
           "ntp1.provider.net",
           "ntp2.provider.net"
          ],
   "ntp::clients": {
                   "10.0.0.0": "255.255.255.0"
                   },
   "postfix::smtprelay": "relay.domain.com",
   "tcpwrappers::allowed_services": {
           "in.tftpd": "ALL"
          },
   "iptables::enabled": true,
   "iptables::manage_rules": true,
   "iptables::exposed_services": {
                           "1": { "53": "udp" },
                           "2": { "53": "tcp" },
                           "3": { "22": "tcp" },
                           "4": { "80": "tcp" },
                           "5": { "8140": "tcp" },
                           "6": { "67": "udp" },
                           "7": { "69": "udp" },
                           "8": { "123": "udp" },
                           "9": { "3000": "tcp" },
                           "10": { "8081": "tcp" }
                                 },
   "selinux::state": "permissive"
}
```

### Satellite
To use these modules in Satellite you will need to follow these steps:

1. Upload the modules
2. Add them to a content view
3. Publish the content view
4. Configure the classes to allow parameter overrides
5. Add the classes where required (config group/host group/host)
6. Override smart class parameters as required

**Please note**
Due to a bug in Foreman, interpretation of '\n' does not currently work in override values. This primarily affects the issue and motd modules, as a work-around please configure the desired text **_inside_** the module and **_disable_** override for those parameters.

A list of classes for a basic RHEL 7 SOE:

|Name|
|:--------|
|abrt_ccpp|
|abrtd|
|abrt_oops|
|access_insights_client|
|accts_lock|
|accts_nologin|
|atd|
|auditd|
|authorized_keys|
|avahi|
|chrony|
|cpumgmt|
|crond|
|dbus|
|device_drivers|
|ebtables|
|extra_facts|
|filesystems|
|firewalld|
|gofer|
|grub2|
|hosts|
|init|
|iptables|
|irqbalance|
|issue|
|kdump|
|limits|
|login_defs|
|lvm2_lvmetad|
|lvm2_monitor|
|mdmonitor|
|mlocate|
|motd|
|ntp|
|ntpdate|
|oddjob|
|pam_login|
|password_quality|
|postfix|
|profiles|
|protocols|
|psacct|
|puppet|
|quota_nld|
|rdisc|
|rhfuncs|
|rhnsd|
|rhsmcertd|
|rsyslog|
|saslauthd|
|scheduling|
|securetty|
|selinux|
|shadow|
|smartd|
|snmp|
|sshd|
|su|
|sudoers|
|sysctl|
|sysstat|
|tcpwrappers|
|tmpfs|
|tuned|
|udev_rules|
|useradd_defs|
|users|
|xinetd|



Example overrides for the RHEL 7 SOE classes above:

|Puppet class|Parameter|Default Value|Override|
|:--------|:--------|:--------|:--------|
|abrt_ccpp|enabled|FALSE|TRUE|
|abrt_oops|enabled|FALSE|TRUE|
|abrtd|enabled|FALSE|TRUE|
|accts_lock|accts|["bin", "daemon", "adm", "lp", "sync", "shutdown", "halt", "mail", "operator", "games", "ftp", "nobody"]|TRUE|
|accts_nologin|accts|["bin", "daemon", "adm", "lp", "sync", "shutdown", "halt", "mail", "operator", "games", "ftp", "nobody"]|TRUE|
|atd|enabled|TRUE|TRUE|
|auditd|enabled|TRUE|TRUE|
|authorized_keys|authorized_keys|{}|TRUE|
|avahi|enabled|FALSE|TRUE|
|chrony|clients|{}|TRUE|
|chrony|enabled|TRUE|TRUE|
|chrony|servers|[]|TRUE|
|cpumgmt|enabled|TRUE|TRUE|
|cpumgmt|package_name|cpuspeed|TRUE|
|cpumgmt|service_name|cpuspeed|TRUE|
|crond|enabled|TRUE|TRUE|
|dbus|enabled|FALSE|TRUE|
|device_drivers|bluetooth_disabled|TRUE|TRUE|
|device_drivers|firewire_core_disabled|TRUE|TRUE|
|device_drivers|net_pf_31_disabled|TRUE|TRUE|
|device_drivers|usb_storage_disabled|TRUE|TRUE|
|ebtables|enabled|FALSE|TRUE|
|filesystems|cramfs_disabled|TRUE|TRUE|
|filesystems|freevxfs_disabled|TRUE|TRUE|
|filesystems|hfs_disabled|TRUE|TRUE|
|filesystems|hfsplus_disabled|TRUE|TRUE|
|filesystems|jffs2_disabled|TRUE|TRUE|
|filesystems|squashfs_disabled|TRUE|TRUE|
|filesystems|udf_disabled|TRUE|TRUE|
|firewalld|enabled|FALSE|TRUE|
|firewalld|zone_add_icmp_block|{}|TRUE|
|firewalld|zone_add_port|{}|TRUE|
|firewalld|zone_add_service|{}|TRUE|
|firewalld|zone_change_interface|{}|TRUE|
|firewalld|zone_create|[]|TRUE|
|firewalld|zone_remove|[]|TRUE|
|firewalld|zone_remove_icmp_block|{}|TRUE|
|firewalld|zone_remove_interface|{}|TRUE|
|firewalld|zone_remove_port|{}|TRUE|
|firewalld|zone_remove_service|{}|TRUE|
|firewalld|zone_set_default|public|TRUE|
|firewalld|zone_set_target|{}|TRUE|
|gofer|enabled|FALSE|TRUE|
|grub2|grub2_password||TRUE|
|hosts|hosts_entries|{}|TRUE|
|iptables|enabled|FALSE|TRUE|
|iptables|exposed_services|{}|TRUE|
|iptables|forward_policy|DROP|TRUE|
|iptables|icmp|TRUE|TRUE|
|iptables|input_policy|DROP|TRUE|
|iptables|manage_rules|FALSE|TRUE|
|iptables|output_policy|ACCEPT|TRUE|
|irqbalance|enabled|auto|TRUE|
|kdump|enabled|FALSE|TRUE|
|limits|core|0|TRUE|
|limits|nofile|8192|TRUE|
|limits|nproc|16384|TRUE|
|login_defs|login_defs_entries|{}|TRUE|
|lvm2_lvmetad|enabled|FALSE|TRUE|
|lvm2_monitor|enabled|FALSE|TRUE|
|mdmonitor|enabled|FALSE|TRUE|
|mlocate|enabled|TRUE|TRUE|
|ntp|clients|{}|TRUE|
|ntp|enabled|FALSE|TRUE|
|ntp|servers|[]|TRUE|
|ntpdate|enabled|FALSE|TRUE|
|oddjob|enabled|TRUE|TRUE|
|password_quality|dcredit|-1|TRUE|
|password_quality|lcredit|-1|TRUE|
|password_quality|minlen|14|TRUE|
|password_quality|ocredit|-1|TRUE|
|password_quality|remembered_passwords|4|TRUE|
|password_quality|ucredit|-1|TRUE|
|postfix|enabled|TRUE|TRUE|
|postfix|epms_admin_email||TRUE|
|postfix|epms_header_content||TRUE|
|postfix|epms_namespace|gov.au|TRUE|
|postfix|epms_subject_content||TRUE|
|postfix|epms_version|2012.3|TRUE|
|postfix|exposed|FALSE|TRUE|
|postfix|relay||TRUE|
|profiles|daemon_umask|22|TRUE|
|profiles|http_proxy||TRUE|
|profiles|https_proxy||TRUE|
|profiles|user_umask|77|TRUE|
|protocols|dccp_disabled|TRUE|TRUE|
|protocols|ipv6_disabled|TRUE|TRUE|
|protocols|rds_disabled|TRUE|TRUE|
|protocols|sctp_disabled|TRUE|TRUE|
|protocols|tipc_disabled|TRUE|TRUE|
|psacct|enabled|TRUE|TRUE|
|puppet|digest_algorithm|sha256|TRUE|
|puppet|enabled|TRUE|TRUE|
|quota_nld|enabled|FALSE|TRUE|
|rdisc|enabled|FALSE|TRUE|
|rhnsd|enabled|FALSE|TRUE|
|rhsmcertd|enabled|FALSE|TRUE|
|rsyslog|enabled|TRUE|TRUE|
|rsyslog|siem_host||TRUE|
|saslauthd|enabled|FALSE|TRUE|
|scheduling|at_allowed_users|["root"]|TRUE|
|scheduling|cron_allowed_users|["root"]|TRUE|
|securetty|allowed_ttys|["console", "tty1"]|TRUE|
|selinux|state|enforcing|TRUE|
|selinux|type|targeted|TRUE|
|smartd|enabled|FALSE|TRUE|
|snmp|snmpd_enabled|FALSE|TRUE|
|snmp|snmptrapd_enabled|FALSE|TRUE|
|sshd|allowed_users|[]|TRUE|
|sshd|enabled|TRUE|TRUE|
|sshd|listen_addresses|[]|TRUE|
|sshd|permit_root_login|no|TRUE|
|sudoers|sudoersd_strict|FALSE|TRUE|
|sysctl|sysctl_entries|{}|TRUE|
|sysstat|enabled|TRUE|TRUE|
|tcpwrappers|allowed_services|{"sshd"=>"ALL"}|TRUE|
|tmpfs|enabled|FALSE|TRUE|
|tuned|enabled|TRUE|TRUE|
|tuned|profile||TRUE|
|useradd_defs|home|/home|TRUE|
|useradd_defs|inactive|30|TRUE|
|useradd_defs|shell|/bin/bash|TRUE|
|users|users|{}|TRUE|
|xinetd|enabled|TRUE|TRUE|
