# Class: kdump
#
#  Enables/disables kdump
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service will be enabled
#   Args: <boolean>
#   Default: true
#
# Sample Usage:
#
#  class { 'kdump': enabled => false, }
#
# Notes:
#
#  Control of the service state has been moved to an exec
#  as the system needs to be booted with the crashkernel
#  argument present for the service to be able to start
#
class kdump(
  $enabled = true,
  $package_name = 'kexec-tools',
  $service_name = 'kdump')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is not supported on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $service_ensure = 'start'
    $service_enable = true
    $package_ensure = 'present'
    $crashkernel_command = 'grubby --update-kernel=ALL --args="crashkernel=auto"'
    $crashkernel_unless = 'grubby --info=ALL | grep -q crashkernel'
    Package["$package_name"] -> File['/etc/kdump.conf']
    File['/etc/kdump.conf'] -> Service["$service_name"]
    File['/etc/kdump.conf'] ~> Service["$service_name"]
    Service["$service_name"] -> Exec['kdump_service_control']
    Service["$service_name"] ~> Exec['kdump_crashkernel_argument']
  }
  else {
    $service_ensure = 'stop'
    $service_enable = false
    $package_ensure = 'absent'
    $crashkernel_command = 'grubby --update-kernel=ALL --remove-args="crashkernel=auto"'
    $crashkernel_unless = 'grubby --info=ALL | grep -qv crashkernel'
    File['/etc/kdump.conf'] -> Package["$package_name"]
    Service["$service_name"] -> File['/etc/kdump.conf']
  }
  package { "$package_name":
    ensure      =>  $package_ensure,
  }
  service { "$service_name":
    ensure      =>  undef,
    enable      =>  $service_enable,
    hasrestart  =>  true,
    hasstatus   =>  true,
  }
  file { '/var/crash':
    ensure   => 'directory',
    group    => 'root',
    mode     => '0700',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'kdump_crash_t',
    seluser  => 'system_u',
  }
  file { '/etc/kdump.conf':
    ensure   => 'file',
    source   => 'puppet:///modules/kdump/kdump.conf',
    group    => 'root',
    mode     => '0644',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'kdump_etc_t',
    seluser  => 'system_u',
  }
  exec { "kdump_crashkernel_argument":
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    command     => $crashkernel_command,
    unless      => $crashkernel_unless,
    refreshonly => true,
  }
  exec { 'kdump_service_control':
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    command     => "systemctl $service_ensure kdump",
    unless      => "grep -qv crashkernel /proc/cmdline",
  }
}
