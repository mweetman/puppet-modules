# Class: smartd
#
#  Enables/disables smartd
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service will be enabled
#   Args: auto, <boolean>
#   Default: false
#
# Sample Usage:
#
#  class { 'smartd': enabled => false, }
#
class smartd(
  $enabled = false,
  $package_name = 'smartmontools',
  $service_name = 'smartd')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  } 
  case $enabled {
    'auto': {
      if $::is_virtual {
        $package_ensure = 'absent'
        $service_ensure = 'stopped'
        $service_enable = false
        Service["$service_name"] -> Package["$package_name"]
      }
      else {
        $package_ensure = 'present'
        $service_ensure = 'running'
        $service_enable = true
        Package["$package_name"] -> Service["$service_name"]
      }
    }
    true: {
      $package_ensure = 'present'
      $service_ensure = 'running'
      $service_enable = true
      Package["$package_name"] -> Service["$service_name"]
    }
    false: {
      $package_ensure = 'absent'
      $service_ensure = 'stopped'
      $service_enable = false
      Service["$service_name"] -> Package["$package_name"]
    }
    default: {
      fail('<*> unrecognised option for smartd $enabled')
    }
  }
  package { "$package_name":
    ensure      =>  $package_ensure,
  }
  service { "$service_name":
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    hasrestart  =>  true,
    hasstatus   =>  true,
  }
}
