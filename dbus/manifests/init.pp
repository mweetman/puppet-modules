# Class: dbus
#
#  Enables/disables dbus services
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service will be enabled
#   Args: <boolean>
#   Default: false
#
# Required by:
#
#  Class['abrtd'], Class['haldaemon']
#
# Sample Usage:
#
#  class { 'dbus': enabled => false, }
#
class dbus (
  $enabled = false,
  $package_name = 'dbus',
  $service_name = 'dbus')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $service_ensure = 'running'
    $service_enable = true
    $package_ensure = 'present'
    Package["$package_name"] -> Service["$service_name"]
  }
  else {
    $service_ensure = 'stopped'
    $service_enable = false
    $package_ensure = undef
    Service["$service_name"] -> Package["$package_name"]
  }
  package { "$package_name":
    ensure  =>  $package_ensure,
  }
  service { "$service_name":
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    hasrestart  =>  true,
    hasstatus   =>  true,
  }
}
