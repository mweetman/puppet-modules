# Class: tcpwrappers
#
#  This module manages tcpwrappers
#
# Parameters:
#
#  [allowed_services]
#   Desc: service/host key/value pairs
#   Args: <hash>
#   Default: empty
#
# Sample Usage:
#
#  class { 'tcpwrappers': }
#
class tcpwrappers(
  $allowed_services = hiera_hash('tcpwrappers::allowed_services', {}))
  {
  file { '/etc/hosts.allow':
    ensure   => 'file',
    content  =>  template('tcpwrappers/hosts.allow.erb'),
    group    => 'root',
    mode     => '0644',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'etc_t',
    seluser  => 'system_u',
  }
  file { '/etc/hosts.deny':
    ensure   => 'file',
    source   => 'puppet:///modules/tcpwrappers/hosts.deny',
    group    => 'root',
    mode     => '0644',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'net_conf_t',
    seluser  => 'system_u',
  }
}
