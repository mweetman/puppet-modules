class { 'postfix':
         enabled              => true,
         relay                => '192.168.1.1',
         epms_header_content  => 'SEC=UNCLASSIFIED',
         epms_subject_content => 'SEC=UNCLASSIFIED',
         epms_admin_email     => 'admin@example.com',
}
