# Class: postfix
#
#  Manages the postfix configuration, package and service
#
# Parameters:
#
#  [enabled]
#   Desc: Determine whether the ntpd service should be installed and running
#   Args: <boolean>
#   Default: true
#
#  [relay]
#   Desc: Provide a relay host to forward mail to 
#   Args: <string>
#   Default: empty
#
#  [exposed]
#   Desc: Whether postfix is listening on all interfaces
#   Args: <boolean>
#   Default: false
#
#  [epms_version]
#   Desc: Email Protective Marking Standard version
#   Args: <string>
#   Default: 2012.3
#
#  [epms_namespace]
#   Desc: Email Protective Marking Standard namespace
#   Args: <string>
#   Default: gov.au
#
#  [epms_header_content]
#   Desc: Email Protective Marking Standard content for X-Protective-Marking header
#   Args: <string>
#   Default: empty
#
#  [epms_subject_content]
#   Desc: Email Protective Marking Standard content for appending to the subject
#   Args: <string>
#   Default: empty
#
#  [epms_admin_email]
#   Desc: Email address component for X-Protective-Marking header
#   Args: <string>
#   Default: empty
#
# Sample Usage:
#
#  class { 'postfix': smtprelay => 'mail.example.com', }
#
class postfix(
  $enabled = true,
  $package_name = 'postfix',
  $service_name = 'postfix',
  $relay = '',
  $exposed = false,
  $epms_version = '2012.3',
  $epms_namespace = 'gov.au',
  $epms_header_content = '',
  $epms_subject_content = '',
  $epms_admin_email = '')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    $file_ensure    = 'file'
    Package["$package_name"] -> File['/etc/postfix/main.cf']
    Package["$package_name"] -> File['/etc/postfix/master.cf']
    File['/etc/postfix/main.cf'] ~> Service["$service_name"]
    File['/etc/postfix/master.cf'] ~> Service["$service_name"]
    File['/etc/postfix/transport'] ~> Exec['postmap_transport']
  }
  else {
    $package_ensure = undef
    $service_ensure = 'stopped'
    $service_enable = false
    $file_ensure    = 'absent'
    Service["$service_name"] -> File['/etc/postfix/main.cf']
    Service["$service_name"] -> File['/etc/postfix/master.cf']
    File['/etc/postfix/main.cf'] -> Package["$package_name"]
    File['/etc/postfix/master.cf'] -> Package["$package_name"]
  }
  if $exposed {
    $interfaces = 'all'
  }
  else {
    $interfaces = 'localhost'
  }
  # resource defaults
  File {
    owner    => 'root',
    group    => 'root',
    mode     => '0644',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'postfix_etc_t',
    seluser  => undef,
  }
  Exec {
    path => '/usr/bin:/bin:/usr/sbin:/sbin',
  }
  # resources
  package { "$package_name":
    ensure  =>  $package_ensure,
  }
  file { '/etc/postfix/main.cf':
    ensure   => $file_ensure,
    content  => template('postfix/main.cf.erb'),
  }
  file { '/etc/postfix/master.cf':
    ensure   => $file_ensure,
    source   => 'puppet:///modules/postfix/master.cf',
  }
  file { '/etc/postfix/transport':
    ensure   => $file_ensure,
    source   => 'puppet:///modules/postfix/transport',
  }
  file { '/etc/postfix/header_checks':
    ensure   => $file_ensure,
    content  => template('postfix/header_checks.erb'),
  }
  exec { 'postmap_transport':
    command     =>  'postmap /etc/postfix/transport',
    onlyif      =>  'test -e /etc/postfix/transport',
    refreshonly =>  true,
  }
  service { "$service_name":
    ensure      => $service_ensure,
    enable      => $service_enable,
    hasrestart  => true,
    hasstatus   => true,
  }
}

