# Class: sshd
#
#  Manages the sshd configuration, package and service
#
# Parameters:
#
#  [enabled]
#   Desc: Determine whether the service should be installed and running
#   Args: <boolean>
#   Default: true
#
#  [permit_root_login]
#   Desc: Whether root is allowed to log in via ssh
#   Args: yes, no
#   Default: no
#
#  [allowed_users]
#   Desc: The users allowed to connect
#   Args: <array>
#   Default: empty
#
#  [listen_addresses]
#   Desc: The ip addresses to listen on
#   Args: <array>
#   Default: empty
#
# Sample Usage:
#
#  class { 'sshd': enabled => false, }
#
class sshd(
  $enabled = true,
  $package_name = 'sshd',
  $service_name = 'sshd',
  $permit_root_login = 'no',
  $allowed_users = [],
  $listen_addresses = [])
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  } 
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    $file_ensure    = 'file'
    Package["$package_name"] -> File['/etc/ssh/sshd_config']
    File['/etc/ssh/sshd_config'] -> Service["$service_name"]
    File['/etc/ssh/sshd_config'] ~> Service["$service_name"]
  }
  else {
    $package_ensure = 'absent'
    $service_ensure = 'stopped'
    $service_enable = false
    $file_ensure    = 'absent'
    Service["$service_name"] -> File['/etc/ssh/sshd_config']
    File['/etc/ssh/sshd_config'] -> Package["$package_name"]
  }
  # resource defaults
  File {
    owner    => 'root',
    group    => 'root',
    mode     => '0600',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'etc_t',
    seluser  => undef, 
  }
  # resources
  package { "$package_name":
    ensure  =>  $package_ensure,
    name    =>  'openssh-server',
  }
  file { '/etc/ssh/sshd_config':
    ensure   => $file_ensure,
    content  => template('sshd/sshd_config.erb'),
  }
  service { "$service_name":
    ensure      => $service_ensure,
    enable      => $service_enable,
    hasrestart  => true,
    hasstatus   => true,
  }
  file { '/etc/pam.d/sshd':
    ensure   => 'file',
    source   => "puppet:///modules/sshd/${::osfamily}/${::operatingsystemmajrelease}/sshd",
    mode     => '0644',
  }
}
