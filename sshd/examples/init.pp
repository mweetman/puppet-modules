class { 'sshd': 
         enabled           => true,
         permit_root_login => 'yes',
         allowed_users     => ['root', 'admin'],
}
