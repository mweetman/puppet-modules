# Class: securetty
#
#  This module manages /etc/securetty
#
# Parameters:
#
#  [allowed_ttys]
#   Desc: terminals that root is permitted to log in on
#   Args: <array>
#   Default: ['console', 'tty1']
#
# Sample Usage:
#
#  class { 'securetty': }
#
class securetty(
  $allowed_ttys = ['console', 'tty1'])
  {
  file { '/etc/securetty':
    ensure   => 'file',
    content  => template('securetty/securetty.erb'),
    group    => 'root',
    mode     => '0600',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'etc_runtime_t',
    seluser  => undef, 
  }
}
