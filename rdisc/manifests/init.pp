# Class: rdisc
#
#  Enables/disables rdisc
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service will be enabled
#   Args: <boolean>
#   Default: false
#
# Sample Usage:
#
#  class { 'rdisc': enabled => false, }
#
class rdisc(
  $enabled = false,
  $package_name = 'iputils',
  $service_name = 'rdisc')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $service_ensure = 'running'
    $service_enable = true
    Package["$package_name"] -> Service["$service_name"]
  }
  else {
    $service_ensure = 'stopped'
    $service_enable = false
  }
  package { "$package_name":
    ensure      =>  'present',
  }
  service { "$service_name":
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    hasrestart  =>  true,
    hasstatus   =>  true,
  }
}
