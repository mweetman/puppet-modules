# Class: ntpdate
#
#  Enables/disables ntpdate
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service will be enabled
#   Args: <boolean>
#   Default: false
#
# Sample Usage:
#
#  class { 'ntpdate': enabled => false, }
#
class ntpdate(
  $enabled = false,
  $package_name = 'ntpdate',
  $service_name = 'ntpdate')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure = 'present'
    $service_enable = true
    Package["$package_name"] -> Service["$service_name"]
  }
  else {
    $package_ensure = undef
    $service_enable = false
    Service["$service_name"] -> Package["$package_name"]
  }
  package { "$package_name":
    ensure      =>  $package_ensure,
  }
  service { "$service_name":
    enable      =>  $service_enable,
    hasrestart  =>  true,
    hasstatus   =>  false,
  }
}
