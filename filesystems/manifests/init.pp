# Class: filesystems
#
#  Disables various filesystems
#
# Parameters:
#
#  [<filesystem>_disabled]
#   Desc: Whether the filesystem is disabled
#   Args: <boolean>
#   Default: true
#
# Sample Usage:
#
#  class { 'filesystems': hfs_disabled => true, }
#
class filesystems(
  $cramfs_disabled = true,
  $freevxfs_disabled = true,
  $hfs_disabled  = true,
  $hfsplus_disabled = true,
  $jffs2_disabled = true,
  $squashfs_disabled = true,
  $udf_disabled = true)
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  $cramfs_file_ensure = $cramfs_disabled ? {
        false          => 'absent',
        default        => 'present',
  }
  $freevxfs_file_ensure = $freevxfs_disabled ? {
        false          => 'absent',
        default        => 'present',
  }
  $hfs_file_ensure = $hfs_disabled ? {
        false          => 'absent',
        default        => 'present',
  }
  $hfsplus_file_ensure = $hfsplus_disabled ? {
        false          => 'absent',
        default        => 'present',
  }
  $jffs2_file_ensure = $jffs2_disabled ? {
        false          => 'absent',
        default        => 'present',
  }
  $squashfs_file_ensure = $squashfs_disabled ? {
        false          => 'absent',
        default        => 'present',
  }
  $udf_file_ensure = $udf_disabled ? {
        false          => 'absent',
        default        => 'present',
  }
  # Resource defaults
  File {
    group    => 'root',
    mode     => '0644',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'modules_conf_t',
    seluser  => undef, 
  }
  # Resources
  file { '/etc/modprobe.d/cramfs.conf':
    ensure   => $cramfs_file_ensure,
    content  => "install cramfs /bin/true\n",
  }
  file { '/etc/modprobe.d/freevxfs.conf':
    ensure   => $freevxfs_file_ensure,
    content  => "install freevxfs /bin/true\n",
  }
  file { '/etc/modprobe.d/hfs.conf':
    ensure   => $hfs_file_ensure,
    content  => "install hfs /bin/true\n",
  }
  file { '/etc/modprobe.d/hfsplus.conf':
    ensure   => $hfsplus_file_ensure,
    content  => "install hfsplus /bin/true\n",
  }
  file { '/etc/modprobe.d/jffs2.conf':
    ensure   => $jffs2_file_ensure,
    content  => "install jffs2 /bin/true\n",
  }
  file { '/etc/modprobe.d/squashfs.conf':
    ensure   => $squashfs_file_ensure,
    content  => "install squashfs /bin/true\n",
  }
  file { '/etc/modprobe.d/udf.conf':
    ensure   => $udf_file_ensure,
    content  => "install udf /bin/true\n",
  }
}
