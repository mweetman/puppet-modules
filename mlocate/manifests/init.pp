# Class: mlocate
#
#  This module manages mlocate configuration
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the cron job will be enabled
#   Args: <boolean>
#   Default: true
#
# Sample Usage:
#
#  class { 'mlocate': }
#
class mlocate(
  $enabled = true,
  $package_name = 'mlocate')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  Package["$package_name"] -> File['/etc/cron.daily/mlocate.cron']
  Package["$package_name"] -> File['/etc/updatedb.conf']
  if $enabled {
    $package_ensure = 'present'
    $file_ensure    = 'file'
  }
  else {
    $package_ensure = 'absent'
    $file_ensure    = 'absent'
  }
  # resource defaults
  File {
    owner    => 'root',
    group    => 'root',
  }
  # resources
  package { "$package_name":
    ensure   => $package_ensure,
  }
  file { '/etc/cron.daily/mlocate.cron':
    ensure   => $file_ensure,
    source   => 'puppet:///modules/mlocate/mlocate.cron',
    mode     => '0700',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'bin_t',
    seluser  => 'unconfined_u',
  }
  file { '/etc/updatedb.conf':
    ensure   => $file_ensure,
    source   => 'puppet:///modules/mlocate/updatedb.conf',
    mode     => '0644',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'etc_t',
    seluser  => 'system_u',
  }
}
