class { 'login_defs':
         login_defs_entries => {
                '001' => {'key' => 'PASS_MAX_DAYS', 'value' => '90'},
                '002' => {'key' => 'PASS_MIN_DAYS', 'value' => '7'},
                '003' => {'key' => 'PASS_MIN_LEN', 'value' => '14'},
                '004' => {'key' => 'PASS_WARN_AGE', 'value' => '7'},
                '005' => {'key' => 'ENCRYPT_METHOD', 'value' => 'SHA512'},
         },
}
