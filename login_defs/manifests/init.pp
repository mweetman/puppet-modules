# Class: login_defs
#
#  This module manages /etc/login.defs
#
# Parameters:
#
#  [login_defs_entries]
#   Desc: key/value pairs for entries in /etc/login.defs
#   Args: <hash>
#   Default: empty
#
# Sample Usage:
#
#  class { 'login_defs': }
#
class login_defs(
  $login_defs_entries = hiera_hash('login_defs::entries', {}))
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  define add_login_defs_entries($key, $value) {
    rhfuncs::insert_key_value { "login_defs_${key}":
      target     => "/etc/login.defs",
      key        => "$key",
      value      => "$value",
      delimiter  => " ",
      spaces     => false,
    }
  }
  create_resources('add_login_defs_entries', $login_defs_entries)
}
