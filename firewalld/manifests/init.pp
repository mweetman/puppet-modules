# Class: firewalld
#
#  Manages firewalld
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service will be enabled
#   Args: <boolean>
#   Default: true
#
#  [zone_create]
#   Desc: zones to create
#   Args: <array>
#   Default: empty
#
#  [zone_remove]
#   Desc: zones to remove
#   Args: <array>
#   Default: empty
#
#  [zone_set_default]
#   Desc: set the default zone
#   Args: <string>
#   Default: public
#
#  [zone_set_target]
#   Desc: set the default target for a zone
#   Args: <hash> - zone: target
#   Default: empty
#
#  [zone_add_service]
#   Desc: add a service to a zone
#   Args: <hash> - zone: service
#   Default: empty
#
#  [zone_remove_service]
#   Desc: remove a service from a zone
#   Args: <hash> - zone: service
#   Default: empty
#
#  [zone_add_port]
#   Desc: add a port to a zone
#   Args: <hash> - zone: port
#   Default: empty
#
#  [zone_remove_port]
#   Desc: remove a port from a zone
#   Args: <hash> - zone: port
#   Default: empty
#
#  [zone_add_icmp_block]
#   Desc: block an icmp type for a zone
#   Args: <hash> - zone: icmptype
#   Default: empty
#
#  [zone_remove_icmp_block]
#   Desc: remove a blocked icmp type from a zone
#   Args: <hash> - zone: icmptype
#   Default: empty
#
#  [zone_change_interface]
#   Desc: bind an interface to a zone
#   Args: <hash> - zone: interface
#   Default: empty
#
#  [zone_remove_interface]
#   Desc: unbind an interface from a zone
#   Args: <hash> - zone: interface
#   Default: empty
#
# Sample Usage:
#
#  class { 'firewalld': enabled => false, }
#
class firewalld(
  $enabled = false,
  $package_name = 'firewalld',
  $service_name = 'firewalld',
  $zone_create = ['public'],
  $zone_remove = [],
  $zone_set_default = 'public',
  $zone_set_target = hiera_hash('firewalld::zone_set_target', {}),
  $zone_add_service = hiera_hash('firewalld::zone_add_service', {}),
  $zone_remove_service = hiera_hash('firewalld::zone_remove_service', {}),
  $zone_add_port = hiera_hash('firewalld::zone_add_port', {}),
  $zone_remove_port = hiera_hash('firewalld::zone_remove_port', {}),
  $zone_add_icmp_block = hiera_hash('firewalld::zone_add_icmp_block', {}),
  $zone_remove_icmp_block = hiera_hash('firewalld::zone_remove_icmp_block', {}),
  $zone_change_interface = hiera_hash('firewalld::zone_change_interface', {}),
  $zone_remove_interface = hiera_hash('firewalld::zone_remove_interface', {}))
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is not supported on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $service_ensure = 'running'
    $service_enable = true
    $package_ensure = 'present'
    $file_ensure = 'directory'
    Package["$package_name"] -> File['/etc/firewalld'] -> Service["$service_name"]
  }
  else {
    $service_ensure = 'stopped'
    $service_enable = false
    $package_ensure = 'absent'
    $file_ensure = 'absent'
    Service["$service_name"] -> File['/etc/firewalld'] -> Package["$package_name"]
  }
  package { "$package_name":
    ensure => $package_ensure,
  }
  service { "$service_name":
    ensure     => $service_ensure,
    enable     => $service_enable,
    hasrestart => true,
    hasstatus  => true,
  }
  file { '/etc/firewalld':
    ensure   => $file_ensure,
    group    => 'root',
    mode     => '0750',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'firewalld_etc_rw_t',
    seluser  => undef, 
    force    => true,
  }
  exec { 'firewalld_reload':
    onlyif	=> 'systemctl -q is-enabled firewalld.service',
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    command     => "firewall-cmd --reload",
    refreshonly => true,
  }
  define firewalld_zone_create() {
    exec { "firewalld_zone_create_${name}":
      path    => '/bin:/usr/bin:/sbin:/usr/sbin',
      command => "firewall-cmd --permanent --new-zone=${name} && systemctl restart firewalld && systemctl reset-failed firewalld",
      unless  => "firewall-cmd --get-zones | grep -qw ${name}",
      require => Service['firewalld'],
    }
  }
  define firewalld_zone_remove() {
    exec { "firewalld_zone_remove_${name}":
      path    => '/bin:/usr/bin:/sbin:/usr/sbin',
      command => "firewall-cmd --permanent --delete-zone=${name}",
      unless  => "firewall-cmd --permanent --get-zones | grep -qwv ${name}",
      notify  => Exec['firewalld_reload'],
      require => Service['firewalld'],
    }
  }
  define firewalld_zone_set_target($zone, $target) {
    exec { "firewalld_${zone}_set_target_${target}":
      path    => '/bin:/usr/bin:/sbin:/usr/sbin',
      command => "firewall-cmd --permanent --zone=${zone} --set-target=${target}",
      unless  => "firewall-cmd --permanent --zone=${zone} --get-target | grep -qw ${target}",
      notify  => Exec['firewalld_reload'],
      require => Exec["firewalld_zone_create_$zone"],
    }
  }
  define firewalld_zone_add_service($zone, $service) {
    exec { "firewalld_${zone}_add_service_${service}":
      path    => '/bin:/usr/bin:/sbin:/usr/sbin',
      command => "firewall-cmd --permanent --zone=${zone} --add-service=${service}",
      unless  => "firewall-cmd -q --permanent --zone=${zone} --query-service=${service}",
      notify  => Exec['firewalld_reload'],
      require => Exec["firewalld_zone_create_$zone"],
    }
  }
  define firewalld_zone_remove_service($zone, $service) {
    exec { "firewalld_${zone}_remove_service_${service}":
      path    => '/bin:/usr/bin:/sbin:/usr/sbin',
      command => "firewall-cmd --permanent --zone=${zone} --remove-service=${service}",
      onlyif  => "firewall-cmd -q --permanent --zone=${zone} --query-service=${service}",
      notify  => Exec['firewalld_reload'],
      require => Exec["firewalld_zone_create_$zone"],
    }
  }
  define firewalld_zone_add_port($zone, $port) {
    exec { "firewalld_${zone}_add_port_${port}":
      path    => '/bin:/usr/bin:/sbin:/usr/sbin',
      command => "firewall-cmd --permanent --zone=${zone} --add-port=${port}",
      unless  => "firewall-cmd -q --permanent --zone=${zone} --query-port=${port}",
      notify  => Exec['firewalld_reload'],
      require => Exec["firewalld_zone_create_$zone"],
    }
  }
  define firewalld_zone_remove_port($zone, $port) {
    exec { "firewalld_${zone}_remove_port_${port}":
      path    => '/bin:/usr/bin:/sbin:/usr/sbin',
      command => "firewall-cmd --permanent --zone=${zone} --remove-port=${port}",
      onlyif  => "firewall-cmd -q --permanent --zone=${zone} --query-port=${port}",
      notify  => Exec['firewalld_reload'],
      require => Exec["firewalld_zone_create_$zone"],
    }
  }
  define firewalld_zone_add_icmp_block($zone, $icmptype) {
    exec { "firewalld_${zone}_add_icmp_block_${icmptype}":
      path    => '/bin:/usr/bin:/sbin:/usr/sbin',
      command => "firewall-cmd --permanent --zone=${zone} --add-icmp-block=${icmptype}",
      unless  => "firewall-cmd -q --permanent --zone=${zone} --query-icmp-block=${icmptype}",
      notify  => Exec['firewalld_reload'],
      require => Exec["firewalld_zone_create_$zone"],
    }
  }
  define firewalld_zone_remove_icmp_block($zone, $icmptype) {
    exec { "firewalld_${zone}_remove_icmp_block_${icmptype}":
      path    => '/bin:/usr/bin:/sbin:/usr/sbin',
      command => "firewall-cmd --permanent --zone=${zone} --remove-icmp-block=${icmptype}",
      onlyif  => "firewall-cmd -q --permanent --zone=${zone} --query-icmp-block=${icmptype}",
      notify  => Exec['firewalld_reload'],
      require => Exec["firewalld_zone_create_$zone"],
    }
  }
  define firewalld_zone_change_interface($zone, $interface) {
    exec { "firewalld_${zone}_add_interface_${interface}":
      path    => '/bin:/usr/bin:/sbin:/usr/sbin',
      command => "firewall-cmd --permanent --zone=${zone} --change-interface=${interface}",
      unless  => "firewall-cmd -q --permanent --zone=${zone} --query-interface=${interface}",
      notify  => Exec['firewalld_reload'],
      require => Exec["firewalld_zone_create_$zone"],
    }
  }
  define firewalld_zone_remove_interface($zone, $interface) {
    exec { "firewalld_${zone}_remove_interface_${interface}":
      path    => '/bin:/usr/bin:/sbin:/usr/sbin',
      command => "firewall-cmd --permanent --zone=${zone} --remove-interface=${interface}",
      onlyif  => "firewall-cmd -q --permanent --zone=${zone} --query-interface=${interface}",
      notify  => Exec['firewalld_reload'],
      require => Exec["firewalld_zone_create_$zone"],
    }
  }
  define firewalld_set_default_zone() {
    exec { "firewalld_set_default_zone_${name}":
      path    => '/bin:/usr/bin:/sbin:/usr/sbin',
      command => "firewall-cmd --set-default-zone=${name}",
      unless  => "firewall-cmd --get-default-zone | grep -qw ${name}",
      notify  => Exec['firewalld_reload'],
      require => Exec["firewalld_zone_create_$name"],
    }
  }
  # instatiate 
  if $enabled {
    firewalld_zone_create{ $zone_create: }
    firewalld_zone_create{ $zone_remove: }
    create_resources('firewalld_zone_set_target', $zone_set_target)
    create_resources('firewalld_zone_add_service', $zone_add_service)
    create_resources('firewalld_zone_remove_service', $zone_remove_service)
    create_resources('firewalld_zone_add_port', $zone_add_port)
    create_resources('firewalld_zone_remove_port', $zone_remove_port)
    create_resources('firewalld_zone_add_icmp_block', $zone_add_icmp_block)
    create_resources('firewalld_zone_remove_icmp_block', $zone_remove_icmp_block)
    create_resources('firewalld_zone_change_interface', $zone_change_interface)
    create_resources('firewalld_zone_remove_interface', $zone_remove_interface)
    firewalld_set_default_zone{ $zone_set_default: }
  }
}
