class { 'firewalld': 
         enabled          => true,
         # ensure 'public' is always in the zone_create list OR that you define zone_set_default to a zone you are creating
         # e.g.
         # zone_create      => ['public', 'POC', 'test'],
         # OR:
         # zone_create      => ['POC', 'test'],
         # zone_set_default => ['POC'],
         zone_create      => ['POC', 'test'],
         zone_set_default => ['POC'],
         zone_add_service => {
                          '001' => {'zone' => 'POC', 'service' => 'smtp'},
                          '002' => {'zone' => 'POC', 'service' => 'ntp'},
                          '003' => {'zone' => 'test', 'service' => 'ftp'},
                          },
         zone_add_port    => {
                         '001' => {'zone' => 'POC', 'port' => '8080/tcp'},
                         '002' => {'zone' => 'test', 'port' => '9000-9001/tcp'}
                          },
}
