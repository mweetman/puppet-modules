# Class: crossroads
#
#  Manages the crossroads configuration, package and service
#
# Parameters:
#
#  [enabled]
#   Desc: Whether crossroads service should be installed and running
#   Args: <boolean>
#   Default: false
#
#  [svc_name]
#   Desc: the service name
#   Args: <string>
#   Default: http
#
#  [svc_port]
#   Desc: the service port
#   Args: <string>
#   Default: 80
#
#  [svc_type]
#   Desc: the service type - http or tcp
#   Args: <string>
#   Default: http
#
#  [http_sticky]
#   Desc: whether session stickiness is active
#   Args: <string>
#   Default: off
#
#  [backends]
#   Desc: group of backend host and hostmatch values
#   Args: <hash>
#   Default: empty
#
#  [svc_webinterface_port]
#   Desc: management interface port
#   Args: <string>
#   Default: 9090
#
#  [svc_webinterface_forwarder_port]
#   Desc: management interface forwarder port
#   Args: <string>
#   Default: 9091
#
#  [svc_webinterface_forwarder_allow]
#   Desc: allowed clients of the management console
#   Args: <string>
#   Default: 127.0.0.1
#
# Sample Usage:
#
#  class { 'crossroads': enabled => false, }
#
class crossroads(
  $enabled = false,
  $package_name = 'crossroads',
  $service_name = 'xrctl',
  $svc_name = 'https',
  $svc_port = '80',
  $svc_type = 'https',
  $http_sticky = 'off',
  $backends = {},
  $svc_webinterface_port = '9090',
  $svc_webinterface_forwarder_port = '9091',
  $svc_webinterface_forwarder_allow = '127.0.0.1')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    $file_ensure    = 'file'
    Package["$package_name"] -> File['/etc/xrctl.xml']
    File['/etc/xrctl.xml'] -> Service["$service_name"]
    File['/etc/xrctl.xml'] ~> Service["$service_name"]
  }
  else {
    $package_ensure = 'absent'
    $service_ensure = 'stopped'
    $service_enable = false
    $file_ensure    = 'absent'
    Service["$service_name"] -> File['/etc/xrctl.xml']
    File['/etc/xrctl.xml'] -> Package["$package_name"]
  }
  # resource defaults
  File {
    owner    => 'root',
    group    => 'root',
    mode     => '0600',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'etc_t',
    seluser  => undef, 
  }
  # resources
  package { "$package_name":
    ensure  =>  $package_ensure,
  }
  file { '/etc/xrctl.xml':
    ensure   => $file_ensure,
    content  => template('crossroads/xrctl.xml.erb'),
  }
  service { "$service_name":
    ensure      => $service_ensure,
    enable      => $service_enable,
    hasrestart  => true,
    hasstatus   => true,
  }
}
