# Class: device_drivers
#
#  Disables various device drivers
#
# Parameters:
#
#  [<driver>_disabled]
#   Desc: Whether the driver is disabled
#   Args: <boolean>
#   Default: true
#
# Sample Usage:
#
#  class { 'device_drivers': bluetooth_disabled => true, }
#
class device_drivers(
  $bluetooth_disabled = true,
  $firewire_core_disabled = true,
  $net_pf_31_disabled = true,
  $usb_storage_disabled = true)
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  $bluetooth_file_ensure = $bluetooth_disabled ? {
        false          => 'absent',
        default        => 'present',
  }
  $firewire_core_file_ensure = $firewire_core_disabled ? {
        false          => 'absent',
        default        => 'present',
  }
  $net_pf_31_file_ensure = $net_pf_31_disabled ? {
        false          => 'absent',
        default        => 'present',
  }
  $usb_storage_file_ensure = $usb_storage_disabled ? {
        false          => 'absent',
        default        => 'present',
  }
  # Resource defaults
  File {
    group    => 'root',
    mode     => '0644',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'modules_conf_t',
    seluser  => undef,
  }
  # Resources
  file { '/etc/modprobe.d/bluetooth.conf':
    ensure   => $bluetooth_file_ensure,
    content  => "install bluetooth /bin/true\n",
  }
  file { '/etc/modprobe.d/firewire_core.conf':
    ensure   => $firewire_core_file_ensure,
    content  => "install firewire_core /bin/true\n",
  }
  file { '/etc/modprobe.d/net-pf-31.conf':
    ensure   => $net_pf_31_file_ensure,
    content  => "install net-pf-31 /bin/true\n",
  }
  file { '/etc/modprobe.d/usb_storage.conf':
    ensure   => $usb_storage_file_ensure,
    content  => "install usb_storage /bin/true\n",
  }
}
