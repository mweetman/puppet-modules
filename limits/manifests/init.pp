# Class: limits
#
#  This module manages /etc/security/limits.d/*
#
# Parameters:
#
#  [nproc]
#   Desc: default maximum number of processes
#   Args: <string>
#   Default: 16384
#
#  [nofile]
#   Desc: default maximum number of file descriptors
#   Args: <string>
#   Default: 8192
#
#  [core]
#   Desc: default maximum size of core files
#   Args: <string>
#   Default: 0
#
# Sample Usage:
#
#  class { 'limits': }
#
class limits(
  $limits_entries = [],
  $nproc = '16384',
  $nofile = '8192',
  $core = '0')
  {
  # resource defaults
  File {
    ensure   => 'file',
    owner    => 'root',
    group    => 'root',
    mode     => '0644',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'etc_t',
    seluser  => undef, 
  }
  # resources
  file { '/etc/security/limits.conf':
    content  => template('limits/limits.conf.erb'),
  }
  file { '/etc/security/limits.d':
    ensure   => 'directory',
    mode     => '0755',
    recurse  => true,
    purge    => true,
  }
  file { '/etc/security/limits.d/90-nproc.conf':
    content  => "* - nproc $nproc\n",
  }
  file { '/etc/security/limits.d/90-nofile.conf':
    content  => "* - nofile $nofile\n",
  }
  file { '/etc/security/limits.d/90-core.conf':
    content  => "* - core $core\n",
  }
}
