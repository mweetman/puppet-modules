# Class: haldaemon
#
#  Enables/disables haldaemon
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service will be enabled
#   Args: <boolean>
#   Default: true
#
# Requires:
#
#  Service[$service_name_dbus]
#
# Sample Usage:
#
#  class { 'haldaemon': enabled => false, }
#
class haldaemon(
  $enabled = true,
  $package_name = 'hal',
  $service_name = 'haldaemon')
  {
  case $::osfamily {
    'RedHat': {
      case $::operatingsystemmajrelease {
        '6': {
          $service_name_dbus = 'messagebus'
        }
        default: {
          $service_name_dbus = 'dbus'
        }
      }
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    Package["$package_name"] -> Service["$service_name"]
  }
  else {
    $package_ensure = 'absent'
    $service_ensure = 'stopped'
    $service_enable = false
    Service["$service_name"] -> Package["$package_name"]
  }
  package { "$package_name":
    ensure      =>  $package_ensure,
  }
  service { "$service_name":
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    hasrestart  =>  true,
    hasstatus   =>  true,
    require     =>  Service["$service_name_dbus"],
  }
}
