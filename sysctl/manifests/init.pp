# Class: sysctl
#
#  This module manages individual entries in /etc/sysctl.conf
#
# Parameters:
#
#  [sysctl_entrie]
#   Desc: key/value pairs to populate /etc/sysctl.conf
#   Args: <hash of hashes>
#   Default: true
#
# Requires:
#
#  rhfuncs::insert_key_value
#
# Sample Usage:
#
#  class { 'sysctl': }
#
class sysctl(
  $sysctl_entries = hiera_hash('sysctl::entries', {}))
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  exec { "/etc/sysctl.conf":
    path        =>  '/usr/bin:/bin:/usr/sbin:/sbin',
    command     =>  'sysctl -p',
    refreshonly =>  true,
  }
  define add_sysctl_entries($key, $value) {
    rhfuncs::insert_key_value { "sysctl_${key}":
      target     => "/etc/sysctl.conf",
      key        => "$key",
      value      => "$value",
      notify     => Exec['/etc/sysctl.conf'],
    }
  }
  create_resources('add_sysctl_entries', $sysctl_entries)
}
