# Class: tuned
#
#  Enables/disables tuned
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service will be enabled
#   Args: <boolean>
#   Default: true
#
#  [profile]
#   Desc: profile to make active, if profile is not provided or is invalid the recommended setting is applied
#   Args: balanced, desktop, latency-performance, network-latency, network-throughput, powersave, throughput-performance, virtual-guest, virtual-host
#   Default: empty
#
# Sample Usage:
#
#  class { 'tuned': enabled => false, }
#
class tuned(
  $enabled = true,
  $package_name = 'tuned',
  $service_name = 'tuned',
  $profile = '')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is not supported on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $service_ensure = 'running'
    $service_enable = true
    $package_ensure = 'present'
    Package["$package_name"] -> File['/etc/tuned/tuned-main.conf']
    File['/etc/tuned/tuned-main.conf'] -> Service["$service_name"]
    File['/etc/tuned/tuned-main.conf'] ~> Service["$service_name"]
    Service["$service_name"] -> Exec['tuned_set_profile']
  }
  else {
    $service_ensure = 'stopped'
    $service_enable = false
    $package_ensure = 'absent'
    Service["$service_name"] -> File['/etc/tuned/tuned-main.conf']
    File['/etc/tuned/tuned-main.conf'] -> Package["$package_name"]
  }
  package { "$package_name":
    ensure      =>  $package_ensure,
  }
  service { "$service_name":
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    hasrestart  =>  true,
    hasstatus   =>  true,
  }
  file { '/etc/tuned/tuned-main.conf':
    ensure   => 'file',
    source   => 'puppet:///modules/tuned/tuned-main.conf',
    group    => 'root',
    mode     => '0644',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'tuned_etc_t',
    seluser  => undef,
  }
  exec { "tuned_set_profile":
    path    => '/bin:/usr/bin:/sbin:/usr/sbin',
    command => "tuned-adm profile $profile || tuned-adm profile $(tuned-adm recommend)",
    unless  => "test -z ${profile} || tuned-adm active | grep -qw $profile",
  }
}
