# Class: ip6tables
#
#  Enables/disables ip6tables
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service will be enabled
#   Args: <boolean>
#   Default: false
#
# Requires:
#
#  Protocol ipv6 enabled
#
# Sample Usage:
#
#  class { 'ip6tables': enabled => false, }
#
class ip6tables(
  $enabled = false)
  {
  case $::osfamily {
    'RedHat': {
      case $::operatingsystemmajrelease {
        '6': {
          $package_name = 'iptables-ipv6'
          $service_name = 'ip6tables'
        }
        default: {
          fail("The ${module_name} module is not supported on ${::osfamily} ${::operatingsystemmajrelease} based systems.")
        }
      }
    }
    default: {
      fail("The ${module_name} module is not supported on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    Package["$package_name"] -> Service["$service_name"]
  }
  else {
    $package_ensure = 'absent'
    $service_ensure = 'stopped'
    $service_enable = false
    Service["$service_name"] -> Package["$package_name"]
  }
  package { "$package_name":
    ensure      =>  $package_ensure,
  }
  service { "$service_name":
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    hasrestart  =>  true,
    hasstatus   =>  true,
  }
}
