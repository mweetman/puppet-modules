# Class: authorized_keys
#
#  This module manages authorized_keys
#
# Parameters:
#
#  [authorized_keys]
#   Desc: hash of parameters including target, owner, group, and keys
#   Args: <hash of hashes>
#   Default: empty
#
# Sample Usage:
#
#  class { 'authorized_keys':
#      authorized_keys => {
#        '001' => {'target' => '/root/.ssh/authorized_keys', 'owner' => 'root', 'group' => 'root', 'keys' => ['<key1>', '<key2>']},
#         '002' => {'target' => '/home/admin/.ssh/authorized_keys', 'owner' => 'admin', 'group' => 'admin', 'keys' => ['<key3>', '<key4>']},                          },
#  }
#
#
class authorized_keys(
  $authorized_keys = hiera_hash('authorized_keys::authorized_keys', {}))
  {
  define add_authorized_keys($target, $owner, $group, $keys) {
    file { "$target":
      ensure   => 'file',
      owner    => "$owner",
      content  => template("authorized_keys/authorized_keys.erb"),
      group    => "$group",
      mode     => '0600',
      selrange => 's0',
      selrole  => 'object_r',
      seltype  => 'ssh_home_t',
      seluser  => undef, 
    }
  }
  create_resources('add_authorized_keys', $authorized_keys)
}
