class { 'authorized_keys':
         authorized_keys => {
             '001' => {'target' => '/root/.ssh/authorized_keys', 'owner' => 'root', 'group' => 'root', 'keys' => ['<key1>', '<key2>']},
             '002' => {'target' => '/home/admin/.ssh/authorized_keys', 'owner' => 'admin', 'group' => 'admin', 'keys' => ['<key3>', '<key4>']},                          },
}
