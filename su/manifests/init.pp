# Class: su
#
#  This module manages permissions and ownership for /bin/su
#
# Sample Usage:
#
#  class { 'su': }
#
class su {
  file { '/bin/su':
    ensure   => 'file',
    group    => 'wheel',
    mode     => '4710',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'su_exec_t',
    seluser  => undef, 
  }
}
