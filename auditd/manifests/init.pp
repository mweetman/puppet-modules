# Class: auditd
#
#  Manages auditd configuration and rules
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service should be enabled
#   Args: <boolean>
#   Default: true
#
# Sample Usage:
#
#  class { 'auditd': enabled => false, }
#
# Notes:
#
#  Changes to the audit rules for a running system will require a reboot due
#  to /etc/audit/rules.d/99_audit_config_immutable.rules
#
class auditd(
  $enabled = true,
  $package_name = 'audit',
  $service_name = 'auditd')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    Package["$package_name"] -> Service["$service_name"]
  }
  else {
    $package_ensure = 'absent'
    $service_ensure = 'stopped'
    $service_enable = false
    Service["$service_name"] -> Package["$package_name"]
  }
  # resource defaults
  File {
    ensure   => 'file',
    owner    => 'root',
    group    => 'root',
    mode     => '0640',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'auditd_etc_t',
    seluser  => undef,
  }
  # resources
  package { "$package_name":
    ensure      =>  $package_ensure,
  }
  service { "$service_name":
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    hasrestart  =>  true,
    hasstatus   =>  true,
  }
  file { '/etc/audit/auditd.conf':
    source   => "puppet:///modules/auditd/auditd.conf"
  }
  file { '/var/log/audit':
    ensure   => 'directory',
    mode     => '0750',
    seltype  => 'auditd_log_t',
  }
  file { '/var/log/audit/audit.log':
    mode     => '0600',
    seltype  => 'auditd_log_t',
  }
  file { '/etc/audisp/plugins.d/syslog.conf':
    source   => "puppet:///modules/auditd/syslog.conf",
    seltype  => 'etc_t',
  }
  # audit rules
  file { '/etc/audit/rules.d/':
    ensure   => 'directory',
    mode     => '0750',
    recurse  => true,
    purge    => true,
  }
  file { '/etc/audit/rules.d/01_audit_config.rules':
    source   => "puppet:///modules/auditd/rules/01_audit_config.rules"
  }
  file { '/etc/audit/rules.d/02_date_time.rules':
    source   => "puppet:///modules/auditd/rules/02_date_time.rules"
  }
  file { '/etc/audit/rules.d/03_user_group.rules':
    source   => "puppet:///modules/auditd/rules/03_user_group.rules"
  }
  file { '/etc/audit/rules.d/04_network.rules':
    source   => "puppet:///modules/auditd/rules/04_network.rules"
  }
  file { '/etc/audit/rules.d/05_password_policy.rules':
    source   => "puppet:///modules/auditd/rules/05_password_policy.rules"
  }
  file { '/etc/audit/rules.d/06_mac.rules':
    source   => "puppet:///modules/auditd/rules/06_mac.rules"
  }
  file { '/etc/audit/rules.d/07_login_logout.rules':
    source   => "puppet:///modules/auditd/rules/07_login_logout.rules"
  }
  file { '/etc/audit/rules.d/08_session.rules':
    source   => "puppet:///modules/auditd/rules/08_session.rules"
  }
  file { '/etc/audit/rules.d/09_dac.rules':
    source   => "puppet:///modules/auditd/rules/09_dac.rules"
  }
  file { '/etc/audit/rules.d/10_file_access.rules':
    source   => "puppet:///modules/auditd/rules/10_file_access.rules"
  }
  file { '/etc/audit/rules.d/11_mount.rules':
    source   => "puppet:///modules/auditd/rules/11_mount.rules"
  }
  file { '/etc/audit/rules.d/12_file_deletion.rules':
    source   => "puppet:///modules/auditd/rules/12_file_deletion.rules"
  }
  file { '/etc/audit/rules.d/13_sudoers.rules':
    source   => "puppet:///modules/auditd/rules/13_sudoers.rules"
  }
  file { '/etc/audit/rules.d/14_sudo_actions.rules':
    source   => "puppet:///modules/auditd/rules/14_sudo_actions.rules"
  }
  file { '/etc/audit/rules.d/15_kernel_modules.rules':
    source   => "puppet:///modules/auditd/rules/15_kernel_modules.rules"
  }
  file { '/etc/audit/rules.d/99_audit_config_immutable.rules':
    source   => "puppet:///modules/auditd/rules/99_audit_config_immutable.rules"
  }
}

