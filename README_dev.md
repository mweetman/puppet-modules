Due to the time it takes to promote a Satellite 6 content view it is preferable to perform Puppet module development and testing outside of Satellite and upload the module once you're happy with the results.

We outline some methods here to aid in Puppet module development at the command line, however this is **not** a replacement for professional training.

### Links
There are a couple of places where you will need to spend a bit of time while developing Puppet modules:
- [Resource Type Reference](https://docs.puppet.com/puppet/latest/reference/type.html)
- [Language: Scope](https://docs.puppet.com/puppet/latest/reference/lang_scope.html)
- [Metaparameter Reference](https://docs.puppet.com/puppet/latest/reference/metaparameter.html)

..and a plug for a script I wrote to perform bulk build and upload of Puppet modules to Satellite 6 :)
- [Pumsu](https://github.com/RedHatSatellite/pumsu)
 

### Recommendations
1. Our most important recommendation is to provision a dedicated Puppet module development machine, it is possible to completely destroy the machine you are working on so using the Satellite host is **not** a Good Idea®. 
2. Second only to the previous recommendation is version control, it is highly recommended that you use a version control repository such as GitLab, Gitorious or Subversion for storing your Puppet modules.
3. Make changes incrementally and test them before continuing, this applies to software development in general but here as well.

### Aliases and Functions
There are a few aliases we can add to ~/.bashrc that make life easier, these are taken from various places:

###### Puppet aliases 
```sh
alias pa='puppet apply -v --modulepath=./'
alias pp='puppet parser validate'
```

###### JSON validation
```sh
jv(){
    cat "$1" | python -m json.tool > /dev/null
}
```

###### JSON pretty print
```sh
jpp(){
    cat "$1" | python -c "import sys; import simplejson as json; data = sys.stdin.read(); print json.dumps(json.loads(data), sort_keys=True, indent=4);"
}
```

###### Puppet (erb) template validation
```sh
function pt()
{
    if [[ -z $1 ]]
    then
        echo "usage: pt <puppet_template_file.erb>"
        return
    fi
    /usr/bin/erb -P -x -T '-' $1 | /usr/bin/ruby -c
}
```

### Examples
Once we have our dedicated module development machine and our helper functions configured we can start working.

###### Generate a new module
We can use Puppet itself to generate the correct directory structure and metadata required, the module name must be prefixed with the authors name:
```sh
~]# puppet module generate mweetman-demo
We need to create a metadata.json file for this module.  Please answer the
following questions; if the question is not applicable to this module, feel free
to leave it blank.

Puppet uses Semantic Versioning (semver.org) to version modules.
What version is this module?  [0.1.0]
--> 

Who wrote this module?  [mweetman]
--> 

What license does this module code fall under?  [Apache 2.0]
--> 

How would you describe this module in a single sentence?
--> A module demonstrating command line development

Where is this module's source code repository?
--> https://gitlab.com/mweetman/puppet-modules

Where can others go to learn more about this module?
--> https://gitlab.com/mweetman/puppet-modules

Where can others go to file issues about this module?
--> https://gitlab.com/mweetman/puppet-modules

----------------------------------------
{
  "name": "mweetman-demo",
  "version": "0.1.0",
  "author": "mweetman",
  "summary": "A module demonstrating command line development",
  "license": "Apache 2.0",
  "source": "https://gitlab.com/mweetman/puppet-modules",
  "project_page": "https://gitlab.com/mweetman/puppet-modules",
  "issues_url": "https://gitlab.com/mweetman/puppet-modules",
  "dependencies": [
    {
      "name": "puppetlabs-stdlib",
      "version_range": ">= 1.0.0"
    }
  ]
}
----------------------------------------

About to generate this metadata; continue? [n/Y]
--> y

Notice: Generating module at /root/mweetman-demo...
Notice: Populating ERB templates...
Finished; module generated in mweetman-demo.
mweetman-demo/Rakefile
mweetman-demo/manifests
mweetman-demo/manifests/init.pp
mweetman-demo/spec
mweetman-demo/spec/classes
mweetman-demo/spec/classes/init_spec.rb
mweetman-demo/spec/spec_helper.rb
mweetman-demo/tests
mweetman-demo/tests/init.pp
mweetman-demo/README.md
mweetman-demo/metadata.json
```

We don't want the author name as part of the directory name at this point as we're not planning to upload this to Puppet Forge, earlier versions of Satellite 6 required that the author name was removed but now it will automatically strip it off if present.
```sh
~]# mv mweetman-demo demo
```

Now that we have our basic structure we need to decide what goes into it, this Demo application is fabricated and unrealistic but should work for our purposes.

###### Files for the Demo application
We can use Puppet to generate a resource definition for existing files:
```sh
~]# puppet resource file /etc/demo.cfg 
file { '/etc/demo.cfg':
  ensure   => 'file',
  content  => '{sha256}56e5bf2514a65d35dbcd0dac3ac5f1384039d818f214e4a392f0a554544dbf52',
  ctime    => '2016-07-07 11:31:44 +1000',
  group    => '0',
  mode     => '600',
  mtime    => '2016-07-07 11:31:44 +1000',
  owner    => '0',
  selrange => 's0',
  selrole  => 'object_r',
  seltype  => 'etc_t',
  seluser  => 'unconfined_u',
  type     => 'file',
}
```

We can paste this output into demo/manifiests/init.pp then tidy it up to ignore the things we don't care about such as type, mtime and ctime. We'll also change the owner and group to the alphabetic id's and add an extra digit to the mode:
```
class demo {

    file { '/etc/demo.cfg':
        ensure   => 'file',
        content  => '{sha256}56e5bf2514a65d35dbcd0dac3ac5f1384039d818f214e4a392f0a554544dbf52',
        group    => 'root',
        mode     => '0600',
        owner    => 'root',
        selrange => 's0',
        selrole  => 'object_r',
        seltype  => 'etc_t',
        seluser  => 'unconfined_u',
    }

}

```

As storing the content directly in the manifest is not very helpful for future editing, we'll copy the file into the module directory structure. There is no directory created to hold files by default but the location is hard-coded:
```sh
~]# mkdir demo/files
~]# cp /etc/demo.cfg demo/files/
```

Then we update the module to point to the copy by changing **content** to **source**:
```
class demo {

    file { '/etc/demo.cfg':
        ensure   => 'file',
        source   => 'puppet:///modules/demo/demo.cfg',
        group    => 'root',
        mode     => '0600',
        owner    => 'root',
        selrange => 's0',
        selrole  => 'object_r',
        seltype  => 'etc_t',
        seluser  => 'unconfined_u',
    }

}
```

As we're playing around with this module we'll want to test various things so let's create an example to apply the class. Keep in mind that demo/manifests/init.pp is **only** a definition, it does not **apply** the class.
```sh
~]# mkdir demo/examples
~]# cat <<'EOF' > demo/examples/init.pp 
class { 'demo':
}
EOF
```

Let's test our demo module so far:
```sh
~]# pa --noop demo/examples/init.pp 
Notice: Compiled catalog for host.lab.internal in environment production in 0.19 seconds
Info: Applying configuration version '1467856999'
Notice: Finished catalog run in 0.32 seconds
```

A successful Puppet run, this means our demo module is syntactically correct.

The **pa** alias we created earlier is configured for use where the Puppet modules you're developing are in the current directory, if you are not in the directory containing your modules you will see errors such as:
```sh
somedir]# pa --noop ~/demo/examples/init.pp 
Error: Puppet::Parser::AST::Resource failed with error ArgumentError: Could not find declared class demo at /root/demo/examples/init.pp:2 on node host.lab.internal
Wrapped exception:
Could not find declared class demo
Error: Puppet::Parser::AST::Resource failed with error ArgumentError: Could not find declared class demo at /root/demo/examples/init.pp:2 on node host.lab.internal

```

Now that we have a successful run we need to test that our module is actually doing something:
```sh
~]# chmod 755 /etc/demo.cfg
~]# pa --noop demo/examples/init.pp 
Notice: Compiled catalog for host.lab.internal in environment production in 0.19 seconds
Info: Applying configuration version '1467857727'
Notice: /Stage[main]/Demo/File[/etc/demo.cfg]/mode: current_value 0755, should be 0600 (noop)
Notice: Class[Demo]: Would have triggered 'refresh' from 1 events
Notice: Stage[main]: Would have triggered 'refresh' from 1 events
Notice: Finished catalog run in 0.27 seconds
```

You can see that Puppet noticed our mode change and would have corrected it if we were running without '--noop', let's allow Puppet to fix the mode:
```sh
~]# pa demo/examples/init.pp 
Notice: Compiled catalog for host.lab.internal in environment production in 0.20 seconds
Info: Applying configuration version '1467857817'
Notice: /Stage[main]/Demo/File[/etc/demo.cfg]/mode: mode changed '0755' to '0600'
Notice: Finished catalog run in 0.24 seconds
~]# ls -l /etc/demo.cfg 
-rw-------. 1 root root 104 Jul  7 11:31 /etc/demo.cfg
```

Cool, now we'll remove the file completely and see if it comes back:
```sh
~]# mv /etc/demo.cfg ~/
~]# pa demo/examples/init.pp 
Notice: Compiled catalog for host.lab.internal in environment production in 0.19 seconds
Info: Applying configuration version '1467857956'
Notice: /Stage[main]/Demo/File[/etc/demo.cfg]/ensure: defined content as '{sha256}56e5bf2514a65d35dbcd0dac3ac5f1384039d818f214e4a392f0a554544dbf52'
Notice: Finished catalog run in 0.32 seconds
~]# cat /etc/demo.cfg 
[main]
# global variables for Demo application
datadir=/var/lib/demo

[server]
interface=eth0
port=8080

```

This all seems to be working as desired so lets move on.

Dropping a file into a specific location is a very simple use case, but what if we want different content in the file depending on which machine we're putting it on? Puppet allows us to do this with templates, which are normal files containing snippets of ruby.

Looking at our demo.cfg file, it seems that we may want the Demo service to run on a different port or only listen on a defined interface. We'll convert our demo.cfg into a template and pass in the variable information as class parameters.

First we'll define our parameters with default values:
```
class demo( 
    interface = 'eth0', 
    port = '8080', 
    datadir = '/var/lib/demo') 
    {
    file { '/etc/demo.cfg':
        ensure   => 'file',
        ...
```

Perform a syntax check:
```sh
~]# pp demo/manifests/init.pp 
Error: Could not parse for environment production: Syntax error at 'interface'; expected ')' at /root/demo/manifests/init.pp:39

```

Oh oh... we forgot variables need to be defined with a preceding '$', adjust accordingly:
```
class demo(
    $interface = 'eth0',
    $port = '8080',
    $datadir = '/var/lib/demo')
    {
    file { '/etc/demo.cfg':
        ensure   => 'file',
        ...
```

.. and syntax check again:
```sh
~]# pp demo/manifests/init.pp 
```

No output is good output in this case.

Now we move the file to the templates directory, the templates directory also doesn't exist by default but is similarly hard-coded. We add the **.erb** extension to make it clear that this is a template:
```sh
~]# mkdir demo/templates
~]# mv demo/files/demo.cfg demo/templates/demo.cfg.erb
```

We edit the new template to replace the current values with variables:
```
[main]
# global variables for Demo application
datadir=<%= @datadir %>

[server]
interface=<%= @interface %>
port=<%= @port %>

```

We'll perform a syntax check on the template to validate our changes:
```sh
~]# pt demo/templates/demo.cfg.erb 
Syntax OK

```

Now we adjust the /etc/demo.cfg file resource in demo/manifests/init.pp to use the template, switching back to using **content** instead of **source**:
```
class demo(
    $interface = 'eth0',
    $port = '8080',
    $datadir = '/var/lib/demo')
    {
    file { '/etc/demo.cfg':
        ensure   => 'file',
        content  => template('demo/demo.cfg.erb'),
        group    => 'root',
        ...
```

Note that the template option takes an argument in the form 'module_name/template_name'.

Performing a Puppet apply now wouldn't achieve anything as the default values we put in for the variables are the same as the current file content so we'll adjust our demo/examples/init.pp file to include parameters with different values:
```
class { 'demo':
         interface => 'eth1',
         port      => '9000',
         datadir   => '/opt/demo',
}
```

Apply our example:
```sh
~]# pa demo/examples/init.pp 
Notice: Compiled catalog for host.lab.internal in environment production in 0.29 seconds
Info: Applying configuration version '1467864018'
Info: /Stage[main]/Demo/File[/etc/demo.cfg]: Filebucketed /etc/demo.cfg to puppet with sum 56e5bf2514a65d35dbcd0dac3ac5f1384039d818f214e4a392f0a554544dbf52
Notice: /Stage[main]/Demo/File[/etc/demo.cfg]/content: content changed '{sha256}56e5bf2514a65d35dbcd0dac3ac5f1384039d818f214e4a392f0a554544dbf52' to '{sha256}6b71a1942839f6e87cbf7350a2e2bf600dc215b2cac141a72bcd3730792d0e8b'
Notice: Finished catalog run in 0.47 seconds
```

.. and confirm the file contents:
```sh
~]# cat /etc/demo.cfg 
[main]
# global variables for Demo application
datadir=/opt/demo

[server]
interface=eth1
port=9000
```

We don't need to actually use an example file to test our changes, Puppet can evaluate(-e) strings that we pass on the command line:
```sh
~]# pa -e 'class {demo: interface => ens0, port => 8443}'
Notice: Compiled catalog for host.lab.internal in environment production in 0.30 seconds
Info: Applying configuration version '1467864238'
Info: /Stage[main]/Demo/File[/etc/demo.cfg]: Filebucketed /etc/demo.cfg to puppet with sum 6b71a1942839f6e87cbf7350a2e2bf600dc215b2cac141a72bcd3730792d0e8b
Notice: /Stage[main]/Demo/File[/etc/demo.cfg]/content: content changed '{sha256}6b71a1942839f6e87cbf7350a2e2bf600dc215b2cac141a72bcd3730792d0e8b' to '{sha256}c7b2261429bfe56c7bff7ef8621a29427a6897af287d96d363eb22c4ca61719e'
Notice: Finished catalog run in 0.31 seconds
~]# cat /etc/demo.cfg 
[main]
# global variables for Demo application
datadir=/var/lib/demo

[server]
interface=ens0
port=8443

```

Looks good, let's change the file options back to their default values:
```sh
~]# pa -e 'class {demo: }'
Notice: Compiled catalog for host.lab.internal in environment production in 0.23 seconds
Info: Applying configuration version '1467864324'
Info: /Stage[main]/Demo/File[/etc/demo.cfg]: Filebucketed /etc/demo.cfg to puppet with sum c7b2261429bfe56c7bff7ef8621a29427a6897af287d96d363eb22c4ca61719e
Notice: /Stage[main]/Demo/File[/etc/demo.cfg]/content: content changed '{sha256}c7b2261429bfe56c7bff7ef8621a29427a6897af287d96d363eb22c4ca61719e' to '{sha256}56e5bf2514a65d35dbcd0dac3ac5f1384039d818f214e4a392f0a554544dbf52'
Notice: Finished catalog run in 0.27 seconds
~]# cat /etc/demo.cfg 
[main]
# global variables for Demo application
datadir=/var/lib/demo

[server]
interface=eth0
port=8080

```

If you are creating custom facts, you can test them using the following process:
```sh
~]# export RUBYLIB=/etc/puppet/environments/production/modules/extra_facts/lib:$RUBYLIB
~]# facter uefi_boot
false
```


Now that we're satisfied with our modules functionality, we can commit our changes and push them to our version control repository.

On the Satellite host we now synchronise the version control repository locally and build the module:
```sh
[root@satellite ~]# git clone https://<host>/<repo>
[root@satellite ~]# cd <repo>
[root@satellite ~]# puppet module build demo
Notice: Building /root/<repo>/demo for release
Module built: /root/<repo>/demo/pkg/mweetman-demo-0.1.0.tar.gz
```

Once built we can upload to a Satellite repository that has the Puppet module type:
```sh
~]# hammer repository upload-content --id <puppet_repo_id> --path /root/<repo>/demo/pkg
Successfully uploaded file 'mweetman-demo-0.1.0.tar.gz'.
```

The module version comes from demo/metadata.json, if we make changes to a module that's already published/uploaded we need to ensure that the version is changed before building:
```json
{
  "name": "mweetman-demo",
  "version": "0.2.0"
  "author": "mweetman",
```

Validate the metadata.json file after editing:
```sh
~]# jv demo/metadata.json 
Expecting , delimiter: line 4 column 3 (char 52)
```

Oops .. a typo, the error message tells us we're  missing a comma:
```json
{
  "name": "mweetman-demo",
  "version": "0.2.0",
  "author": "mweetman",
  "summary": "A module demonstrating command line development",
  "license": "Apache 2.0",
```

Revalidate and as before, no output is good.


Hopefully this guide will assist you with developing your Puppet modules, please feel free to open an issue with comments or suggestions.
