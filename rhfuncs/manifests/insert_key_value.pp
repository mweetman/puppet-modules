# Definition:
# - Requires GNU sed, perl support can be added if required
# - To use this function, please ensure you add an exec to the calling module
#   with title matching $target, this is required to feed back refresh events.
#
# Eg.
#  exec { "/etc/sysctl.conf":
#    path        =>  '/usr/bin:/bin:/usr/sbin:/sbin',
#    command     =>  'sysctl -p',
#    refreshonly =>  true,
#  }
#
# Parameters:
# - The $target file
# - The $key
# - The $value
# - The $executable to use: [sed]
#
# Actions:
# - Ensures a key value pair are present in a given file
#
# Requires:
# - GNU sed
#
# Sample Usage:
#  rhfuncs::insert_key_value { 'kernel.exec-shield':
#    target   => '/etc/sysctl.conf',
#    key      => 'kernel.exec-shield',
#    value    => '1',
#    notify   => Exec[/etc/sysctl.conf'],
#  }
#
define rhfuncs::insert_key_value ($target, $key, $value, $executable = 'sed', $delimiter = '=', $notify = undef, $spaces = true) {
  if $spaces {
    $space = ' '
  }
  else {
    $space = undef
  }
  case $executable {
    # Add other executable cases here if required - i.e. perl
    default: {
      exec { "$title":
        provider  =>  'shell',
        path      =>  '/usr/bin:/bin:/usr/sbin:/sbin',
        command   =>  "(grep -q '^${key}' ${target} && sed -i 's|^${key}.*|${key}${space}${delimiter}${space}${value}|' ${target}) || echo '${key}${space}${delimiter}${space}${value}' >> ${target}",
        unless    =>  "grep -q '^${key}${space}${delimiter}${space}${value}$' ${target}",
        notify    =>  $notify
      }
    }
  }
}
