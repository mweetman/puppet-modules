# Definition: insert_line
# - Uses perl by default for its tab support
# - Supports GNU sed if perl is not available
#
# Parameters:
# - The $position to insert the line at
# - The $target file
# - The $content of the line
# - The $executable to use: [perl, sed]
#
# Actions:
# - Inserts a line in a given file at a given position
#
# Requires:
# - perl or:
# - GNU sed
#
# Sample Usage:
#  rhfuncs::insert_line { 'pluginsync':
#    position => '2',
#    target   => '/etc/puppet/puppet.conf',
#    content  => '\tpluginsync = true',
#  }
#
define rhfuncs::insert_line ($position, $target, $content, $executable = undef) {
  case $executable {
    'perl': {
      exec { "$title":
        path      =>  '/usr/bin:/bin:/usr/sbin:/sbin',
        command   =>  "perl -pi -le 'print \"${content}\" if $. == ${position}' ${target}",
        unless    =>  "perl -n -e 'print if $. == ${position}' ${target} | grep -q '^${content}$'",
      }
    }
    default: {
      # content with leading whitespace does not work with sed, use perl instead
      exec { "$title":
        path      =>  '/usr/bin:/bin:/usr/sbin:/sbin',
        command   =>  "sed -i '${position}i${content}' ${target}",
        unless    =>  "echo $(sed -n '${position}p' ${target}) | grep -q '^${content}$'",
      }
    }
  }
}
