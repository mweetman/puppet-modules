# physpages.rb
require 'facter'
Facter.add(:physpages) do
  setcode do
     Facter::Util::Resolution.exec('getconf _PHYS_PAGES')
  end
end
