# recshmall.rb
# generates recommended value for SHMALL based on %80 of pysical memory
require 'facter'
Facter.add(:recshmall) do
  setcode do
    physpgs = Facter.value(:physpages)
    if physpgs
      recshmall = Integer(physpgs) / 10 * 8
    end
    recshmall
  end
end
