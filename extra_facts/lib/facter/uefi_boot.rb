# uefi_boot.rb
require 'facter'
Facter.add(:uefi_boot) do
  confine :kernel => 'Linux'
  setcode do
    Facter::Util::Resolution.exec('test -e /sys/firmware/efi && echo "true" || echo "false"')
  end
end
