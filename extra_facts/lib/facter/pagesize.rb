# pagesize.rb
require 'facter'
Facter.add(:pagesize) do
  setcode do
     Facter::Util::Resolution.exec('getconf PAGE_SIZE')
  end
end
