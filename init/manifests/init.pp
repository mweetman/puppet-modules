# Class: init
#
#  This module manages /etc/sysconfig/init
#
# Sample Usage:
#
#  class { 'init': }
#
class init {
  file { '/etc/sysconfig/init':
    ensure   => 'file',
    source   => "puppet:///modules/init/${::osfamily}/${::operatingsystemmajrelease}/init",
    group    => 'root',
    mode     => '0644',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'etc_t',
    seluser  => undef, 
  }
}
