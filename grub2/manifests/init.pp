# Class: grub2
#
#  Manages grub2
#
# Parameters:
#
#  [grub2_password]
#   Desc: Encrypted password for grub2 superuser, generate with grub2-mkpasswd-pbkdf2
#   Args: <string>
#   Default: empty
#
# Sample Usage:
#
#  class { 'grub2': }
#
class grub2(
  $grub2_password = '')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is not supported on ${::osfamily} based systems.")
    }
  }
  File['user.cfg'] ~> Exec['grub2-mkconfig']
  Exec['grub2_enable_auditing'] ~> Exec['grub2-mkconfig']
  Exec['grub2_enable_selinux'] ~> Exec['grub2-mkconfig']
  if $uefi_boot == 'true' {
    $grub_prefix = '/boot/efi/EFI/redhat'
  }
  else {
    $grub_prefix = '/boot/grub2'
  }
  file { 'grub.cfg':
    path     => "${grub_prefix}/grub.cfg",
    ensure   => 'file',
    group    => 'root',
    mode     => '0600',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'boot_t',
    seluser  => 'unconfined_u',
  }
  file { 'user.cfg':
    path     => "${grub_prefix}/user.cfg",
    ensure   => 'file',
    content  => template('grub2/user.cfg.erb'),
    group    => 'root',
    mode     => '0600',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'boot_t',
    seluser  => 'unconfined_u',
  }
  exec { 'grub2_enable_auditing':
    path    => '/bin:/usr/bin:/sbin:/usr/sbin',
    command => 'sed -i \'s|^GRUB_CMDLINE_LINUX="\(.*\)"|GRUB_CMDLINE_LINUX="\1 audit=1"|\' /etc/default/grub',
    unless  => "grep -q 'audit=1' /etc/default/grub",
  }
  exec { 'grub2_enable_selinux':
    path    => '/bin:/usr/bin:/sbin:/usr/sbin',
    command => 'sed -i \'s|^GRUB_CMDLINE_LINUX="\(.*\)"|GRUB_CMDLINE_LINUX="\1 selinux=1"|\' /etc/default/grub',
    unless  => "grep -q 'selinux=1' /etc/default/grub",
  }
  exec { 'grub2-mkconfig':
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    command     => "grub2-mkconfig -o ${grub_prefix}/grub.cfg",
    refreshonly => true,
  }
}
