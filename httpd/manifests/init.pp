# Class: httpd
#
#  Manages the httpd configuration, package and service
#
# Parameters:
#
#  [enabled]
#   Desc: Determine whether the httpd service should be installed and running
#   Args: <boolean>
#   Default: false
#
# Sample Usage:
#
#  class { 'httpd': enabled => false, }
#
class httpd(
  $enabled = false,
  $package_name     = 'httpd',
  $package_name_ssl = 'mod_ssl',
  $service_name     = 'httpd')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    $file_ensure    = 'file'
    Package["$package_name"] -> Service["$service_name"]
    Package["$package_name_ssl"] -> Service["$service_name"]
  }
  else {
    $package_ensure = 'absent'
    $service_ensure = 'stopped'
    $service_enable = false
    $file_ensure    = 'absent'
    Service["$service_name"] -> Package["$package_name"]
    Service["$service_name"] -> Package["$package_name_ssl"]
  }
  # resources
  package { "$package_name":
    ensure  =>  $package_ensure,
  }
  package { "$package_name_ssl":
    ensure  =>  $package_ensure,
  }
  service { "$service_name":
    ensure      => $service_ensure,
    enable      => $service_enable,
    hasrestart  => true,
    hasstatus   => true,
  }
}
