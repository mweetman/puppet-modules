# Class: accts_lock
#
#  This module ensures defined accounts are locked
#
# Parameters:
#
#  [accts]
#   Desc: System accounts to lock
#   Args: <array>
#   Default: empty
#
# Sample Usage:
#
#  class { 'accts_lock': }
#
class accts_lock(
  $accts = [])
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  define lock_accts() {
    exec { "accts_lock_${name}":
      path    =>  '/bin:/usr/bin:/sbin:/usr/sbin',
      command =>  "usermod -L ${name}",
      unless  =>  "grep ^${name}: /etc/shadow | cut -d: -f2 | grep ^!",
    }
  }
  lock_accts{ $accts: }
}

