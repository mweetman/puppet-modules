# Class: abrt_ccpp
#
#  Manages the abrt-ccpp service
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the abrt-ccpp service should be enabled
#   Args: <boolean>
#   Default: false
#
# Requires:
#
#  Service['abrtd']
#
# Sample Usage:
#
#  class { 'abrt_ccpp': enabled => false,}
#
class abrt_ccpp(
  $enabled = false,
  $package_name = 'abrt-addon-ccpp',
  $service_name = 'abrt-ccpp')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    Package["$package_name"] -> Service["$service_name"]
  }
  else {
    $package_ensure = 'absent'
    $service_ensure = 'stopped'
    $service_enable = false
    Service["$service_name"] -> Package["$package_name"]
  }
  # resources
  package { "$package_name":
    ensure      =>  $package_ensure,
  }
  service { "$service_name":
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    require     =>  Service['abrtd'],
  }
}
