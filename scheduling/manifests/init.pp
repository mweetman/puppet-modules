# Class: scheduling
#
#  This module manages permissions for cron and at files/directories
#
# Parameters:
#
#  [at_allowed_users]
#   Desc: List of users permitted to manage at jobs
#   Args: <array>
#   Default: ['root']

#  [cron_allowed_users]
#   Desc: List of users permitted to manage cron jobs
#   Args: <array>
#   Default: ['root']

# Sample Usage:
#
#  class { 'scheduling': }
#
class scheduling(
  $at_allowed_users = ['root'],
  $cron_allowed_users = ['root'])
  {
  # resource defaults
  File {
    group    => 'root',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seluser  => undef, 
  }
  # resources
  file { '/etc/cron.d':
    ensure   => 'directory',
    mode     => '0700',
    seltype  => 'system_cron_spool_t',
  }
  file { '/etc/cron.hourly':
    ensure   => 'directory',
    mode     => '0700',
    seltype  => 'bin_t',
  }
  file { '/etc/cron.daily':
    ensure   => 'directory',
    mode     => '0700',
    seltype  => 'bin_t',
  }
  file { '/etc/cron.weekly':
    ensure   => 'directory',
    mode     => '0700',
    seltype  => 'bin_t',
  }
  file { '/etc/cron.monthly':
    ensure   => 'directory',
    mode     => '0700',
    seltype  => 'bin_t',
  }
  file { '/etc/crontab':
    ensure   => 'file',
    mode     => '0600',
    seltype  => 'system_cron_spool_t',
  }
  file { '/etc/anacrontab':
    ensure   => 'file',
    mode     => '0600',
    seltype  => 'etc_t',
  }
  file { '/etc/cron.allow':
    ensure   => 'file',
    content  => template('scheduling/cron.allow.erb'),
    mode     => '0400',
    seltype  => 'etc_t',
  }
  file { '/etc/at.allow':
    ensure   => 'file',
    content  => template('scheduling/at.allow.erb'),
    mode     => '0400',
    seltype  => 'etc_t',
  }
  file { '/etc/cron.deny':
    ensure   => 'absent',
  }
  file { '/etc/at.deny':
    ensure   => 'absent',
  }
}
