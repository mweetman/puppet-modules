# Class: lvm2_lvmetad
#
#  Enables/disables lvm2-lvmetad
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service will be enabled
#   Args: <boolean>
#   Default: false
#
# Sample Usage:
#
#  class { 'lvm2_lvmetad': enabled => false, }
#
class lvm2_lvmetad (
  $enabled      = false,
  $package_name = 'lvm2',
  $service_name = 'lvm2-lvmetad')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    Package["$package_name"] -> Service["$service_name"]
  }
  else {
    $package_ensure = undef
    $service_ensure = 'stopped'
    $service_enable = false
  }
  service { "$service_name":
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    hasrestart  =>  true,
    hasstatus   =>  true,
    require     =>  Package[$package_name]
  }
}
