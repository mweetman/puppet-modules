These Puppet modules were created as a learning experience after Puppet Master training to replace a complex kickstart post section. They were originally designed to function with Hiera but have been updated to be compatible with Red Hat Satellite 6.

As you can see from the example Hiera config below, there is no intelligence in the modules regarding the supported OS major version/s - it is up to the ENC to provide this.


/etc/puppet/hiera.yaml:
```yaml
---
:backends:
  - json
:json:
  :datadir: /etc/puppet/hiera
:hierarchy:
  - "node/%{::fqdn}"
  - "roles/%{::role_web}"
  - "roles/%{::role_app}"
  - "roles/%{::role_db}"
  - "os/%{::osfamily}/%{::operatingsystemmajrelease}"
  - "os/%{::osfamily}"
  - common
```

/etc/puppet/hiera/common.json:
```json
{
   "-=comment_01=-": "Classes to include",
   "classes": [
    "abrtd",
    "abrt_ccpp",
    "abrt_oops",
    "accts_lock",
    "accts_nologin",
    "atd",
    "auditd",
    "cpumgmt",
    "crond",
    "extra_facts",
    "init",
    "iptables",
    "irqbalance",
    "issue",
    "limits",
    "login_defs",
    "lvm2_monitor",
    "mdmonitor",
    "messagebus",
    "mlocate",
    "ntp",
    "ntpdate",
    "pam_login",
    "pam_sshd",
    "pam_system_auth",
    "postfix",
    "profiles",
    "protocols",
    "psacct",
    "puppet",
    "quota_nld",
    "rdisc",
    "rhfuncs",
    "rsyslog",
    "saslauthd",
    "scheduling",
    "securetty",
    "selinux",
    "smartd",
    "snmp",
    "sshd",
    "su",
    "sudoers",
    "sysctl",
    "sysstat",
    "tcpwrappers",
    "xinetd"
    ],
   "-=comment_02=-": "Service configuration",
   "abrtd::enabled": true,
   "abrt_ccpp::enabled": false,
   "abrt_oops::enabled": false,
   "atd::enabled": false,
   "auditd::enabled": true,
   "cpumgmt::enabled": true,
   "crond::enabled": true,
   "iptables::enabled": false,
   "irqbalance::enabled": "auto",
   "lvm2_monitor::enabled": false,
   "mdmonitor::enabled": false,
   "messagebus::enabled": false,
   "ntp::enabled": false,
   "ntpdate::enabled": false,
   "postfix::enabled": true,
   "psacct::enabled": false,
   "puppet::enabled": true,
   "quota_nld::enabled": false,
   "rdisc::enabled": false,
   "rsyslog::enabled": true,
   "saslauthd::enabled": false,
   "smartd::enabled": false,
   "snmp::snmpd_enabled": false,
   "snmp::snmptrapd_enabled": false,
   "snmptrapd::enabled": false,
   "sshd::enabled": true,
   "sshd::allowed_users": "dlops root",
   "sysstat::enabled": true,
   "xinetd::enabled": true,
   "-=comment_03=-": "Configuration for hardening classes that require it",
   "accts_lock::accts": [ "bin",
                          "daemon",
                          "adm",
                          "lp",
                          "sync",
                          "shutdown",
                          "halt",
                          "mail",
                          "operator",
                          "games",
                          "ftp",
                          "nobody" ],
   "accts_nologin::accts": [ "bin",
                          "daemon",
                          "adm",
                          "lp",
                          "sync",
                          "shutdown",
                          "halt",
                          "mail",
                          "operator",
                          "games",
                          "ftp",
                          "nobody" ],
   "protocols::dccn_disabled": true,
   "protocols::ipv6_disabled": true,
   "protocols::rds_disabled": true,
   "protocols::sctp_disabled": true,
   "protocols::tipc_disabled": true,
   "mlocate::enabled": true,
   "selinux::state": "disabled",
   "tcpwrappers::services": {
     "sshd": "ALL"
    }
}
```

/etc/puppet/hiera/os/RedHat/6.json:
```json
{
   "-=comment_01=-": "Classes to include",
   "classes": [
                "haldaemon",
                "ip6tables",
                "netfs",
                "restorecond"
    ],
   "-=comment_02=-": "Service configuration",
   "accts_lock::accts": [ "uucp",
                          "gopher" ],
   "accts_nologin::accts": [ "uucp",
                          "gopher" ],
   "haldaemon::enabled": false,
   "ip6tables::enabled": false,
   "netfs::enabled": true,
   "restorecond::enabled": false
}
```

/etc/puppet/hiera/os/RedHat/7.json:
```json
{
   "-=comment_01=-": "Classes to include",
   "classes": [
    ],
   "-=comment_02=-": "Service configuration",
   "cpumgmt::package_name": "kernel-tools",
   "cpumgmt::service_name": "cpupower"
}
```

/etc/puppet/hiera/node/somehost.domain.com.json:
```json
{
   "ntp::servers": [
           "ntp1.provider.net",
           "ntp2.provider.net"
          ],
   "ntp::clients": {
                   "10.0.0.0": "255.255.255.0"
                   },
   "postfix::smtprelay": "relay.domain.com",
   "tcpwrappers::services": {
           "in.tftpd": "ALL"
          },
   "iptables::enabled": true,
   "iptables::manage_rules": true,
   "iptables::exposed_services": {
                           "1": { "53": "udp" },
                           "2": { "53": "tcp" },
                           "3": { "22": "tcp" },
                           "4": { "80": "tcp" },
                           "5": { "8140": "tcp" },
                           "6": { "67": "udp" },
                           "7": { "69": "udp" },
                           "8": { "123": "udp" },
                           "9": { "3000": "tcp" },
                           "10": { "8081": "tcp" }
                                 },
   "selinux::state": "permissive"
}
```

