# Class: profiles
#
#  This module manages files related to login environments
#
# Parameters:
#
#  [daemon_umask]
#   Desc: umask for daemon processes
#   Args: <string>
#   Default: 022
#
#  [user_umask]
#   Desc: umask for users
#   Args: <string>
#   Default: 077
#
#  [http_proxy]
#   Desc: System wide http_proxy variable
#   Args: <string>
#   Default: empty
#
#  [https_proxy]
#   Desc: System wide https_proxy variable
#   Args: <string>
#   Default: empty
#
# Sample Usage:
#
#  class { 'profiles': }
#
class profiles(
  $daemon_umask = '022',
  $user_umask = '077',
  $http_proxy = '',
  $https_proxy = '')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  # resource defaults
  File {
    group    => 'root',
    mode     => '0644',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'etc_t',
    seluser  => 'system_u',
  }
  # resources
  file { '/etc/bashrc':
    ensure   => 'file',
    source   => 'puppet:///modules/profiles/bashrc',
  }
  file { '/etc/profile':
    ensure   => 'file',
    source   => 'puppet:///modules/profiles/profile',
  }
  file { '/etc/csh.cshrc':
    ensure   => 'file',
    source   => 'puppet:///modules/profiles/csh.cshrc',
  }
  file { '/etc/csh.login':
    ensure   => 'file',
    source   => 'puppet:///modules/profiles/csh.login',
  }
  file { '/etc/profile.d/site_defaults.sh':
    ensure   => 'file',
    content  => template('profiles/site_defaults.sh.erb'),
  }
  file { '/etc/profile.d/autologout.sh':
    ensure   => 'file',
    source   => 'puppet:///modules/profiles/autologout.sh',
  }
  file { '/etc/profile.d/system_proxy.sh':
    ensure   => 'file',
    content  => template('profiles/system_proxy.sh.erb'),
  }
  rhfuncs::insert_key_value { "daemon_umask_functions":
    target     => "/etc/init.d/functions",
    key        => "umask",
    value      => "$daemon_umask",
    delimiter  => " ",
    spaces     => false,
  }
}
