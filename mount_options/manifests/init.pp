# Class: mount_options
#
#  This module manages mount options in /etc/fstab
#
# Sample Usage:
#
#  class { 'mount_options': }
#
class mount_options(
  $mount_options_entries = hiera_hash('mount_options::entries', {}))
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is not supported on ${::osfamily} based systems.")
    }
  }

  define set_mount_option($mount_point, $option) {
    $target = "/etc/fstab"
    exec { "mount_options-${mount_point}":
      provider => shell,
      path    => '/bin:/usr/bin:/sbin:/usr/sbin',
      command => "options=$(grep '${mount_point}' ${target} | awk '{sub(\$4, \$4\",${option}\",\$0); print \$0}') && sed -i \"s|^.*\s${mount_point}\s.*|\$options|\" ${target}",
      unless  => "grep '${mount_point}' ${target} | awk '{print \$4}' | grep -wq ${option}",
    }
  }
  create_resources('set_mount_option', $mount_options_entries)
}
