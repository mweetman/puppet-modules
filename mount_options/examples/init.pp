class { 'mount_options':
         mount_options_entries => {
                    'tmp_nodev' => {'mount_point' => '/tmp', 'option' => 'nodev'},
                    'boot_noexec' => {'mount_point' => '/boot', 'option' => 'noexec'},
                    },

}
