# Class: pam_login
#
#  This module manages /etc/pam.d/login
#
# Sample Usage:
#
#  class { 'pam_login': }
#
class pam_login {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  file { '/etc/pam.d/login':
    ensure   => 'file',
    source   => "puppet:///modules/pam_login/${::osfamily}/${::operatingsystemmajrelease}/login",
    group    => 'root',
    mode     => '0644',
    owner    => 'root',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'etc_t',
    seluser  => undef, 
  }
}
