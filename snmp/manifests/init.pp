# Class: snmp
#
#  Manages snmp services
#
# Parameters:
#
#  [snmpd_enabled]
#   Desc: Whether the service will be enabled
#   Args: <boolean>
#   Default: false
#
#  [snmptrapd_enabled]
#   Desc: Whether the service will be enabled
#   Args: <boolean>
#   Default: false
#
# Sample Usage:
#
#  class { 'snmp': snmpd_enabled => false, snmptrapd_enabled => false }
#
class snmp(
  $snmpd_enabled = false,
  $snmptrapd_enabled = false,
  $package_name      = 'net-snmp',
  $service_name      = 'snmpd',
  $service_name_trap = 'snmptrapd')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  } 
  if $snmpd_enabled {
    $snmpd_service_ensure = 'running'
    $snmpd_service_enable = true
    Package["$package_name"] -> Service["$service_name"]
  }
  else {
    $snmpd_service_ensure = 'stopped'
    $snmpd_service_enable = false
    Service["$service_name"] -> Package["$package_name"]
  }
  if $snmptrapd_enabled {
    $snmptrapd_service_ensure = 'running'
    $snmptrapd_service_enable = true
    Package["$package_name"] -> Service["$service_name_trap"]
  }
  else {
    $snmptrapd_service_ensure = 'stopped'
    $snmptrapd_service_enable = false
    Service["$service_name_trap"] -> Package["$package_name"]
  }
  if (!$snmpd_enabled and !$snmptrapd_enabled) {
    $package_ensure = 'absent'
  }
  else {
    $package_ensure = 'present'
  }
  package { "$package_name":
    ensure      =>  $package_ensure,
  }
  service { "$service_name":
    ensure      =>  $snmpd_service_ensure,
    enable      =>  $snmpd_service_enable,
    hasrestart  =>  true,
    hasstatus   =>  true,
  }
  service { "$service_name_trap":
    ensure      =>  $snmptrapd_service_ensure,
    enable      =>  $snmptrapd_service_enable,
    hasrestart  =>  true,
    hasstatus   =>  true,
  }
}
