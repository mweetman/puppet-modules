# Class: irqbalance
#
#  Enables/disables irqbalance
#
# Parameters:
#
#  [enabled]
#   Whether the service will be enabled
#   Args: auto, <boolean>
#   Default: auto - (service will be enabled if on a SMP system)
#
# Sample Usage:
#
#  class { 'irqbalance': enabled => false, }
#
class irqbalance(
  $enabled = 'auto',
  $package_name = 'irqbalance',
  $service_name = 'irqbalance')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  case $enabled {
    'auto': {
      if $::physicalprocessorcount > 1 {
        $package_ensure = 'present'
        $service_ensure = 'running'
        $service_enable = true
        Package["$package_name"] -> Service["$service_name"]
      }
      else {
        $package_ensure = 'absent'
        $service_ensure = 'stopped'
        $service_enable = false
        Service["$service_name"] -> Package["$package_name"]
      }
    }
    true: {
      $package_ensure = 'present'
      $service_ensure = 'running'
      $service_enable = true
      Package["$package_name"] -> Service["$service_name"]
    }
    false: {
      $package_ensure = 'absent'
      $service_ensure = 'stopped'
      $service_enable = false
      Service["$service_name"] -> Package["$package_name"]
    }
    default: {
      fail("<*> unrecognised value for irqbalance \$enabled: $enabled")
    }
  }
  package { "$package_name":
    ensure      =>  $package_ensure,
  }
  service { "$service_name":
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    hasrestart  =>  true,
    hasstatus   =>  true,
  }
}
