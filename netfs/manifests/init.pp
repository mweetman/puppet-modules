# Class: netfs
#
#  Enables/disables netfs
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the service will be enabled
#   Args: <boolean>
#   Default: true
#
# Sample Usage:
#
#  class { 'netfs': enabled => false, }
#
class netfs(
  $enabled = true,
  $service_name = 'netfs')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $service_ensure = 'running'
    $service_enable = true
  }
  else {
    $service_ensure = 'stopped'
    $service_enable = false
  }
  service { "$service_name":
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    hasrestart  =>  true,
    hasstatus   =>  true,
  }
}
