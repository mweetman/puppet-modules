# Class: password_quality
#
#  This module manages password quality
#
# Parameters:
#
#  [remembered_passwords]
#   Desc: Number of passwords to remember
#   Args: <string>
#   Default: 4
#
#  [minlen]
#   Desc: minimum password length
#   Args: <string>
#   Default: 14
#
#  [dcredit]
#   Desc: credit toward length for including digits
#   Args: <string>
#   Default: -1
#
#  [ucredit]
#   Desc: credit toward length for including upper-case letters
#   Args: <string>
#   Default: -1
#
#  [ocredit]
#   Desc: credit toward length for including special characters
#   Args: <string>
#   Default: -1
#
#  [lcredit]
#   Desc: credit toward length for including lower-case letters
#   Args: <string>
#   Default: -1
#
# Sample Usage:
#
#  class { 'password_quality': }
#
class password_quality(
  $remembered_passwords = '4',
  $minlen = '14',
  $dcredit = '-1',
  $ucredit = '-1',
  $ocredit = '-1',
  $lcredit = '-1')
  {
  case $::osfamily {
    'RedHat': {
      case $::operatingsystemmajrelease {
        '6': {
          $file_ensure = 'absent'
        }
        default: {
          $file_ensure = 'file'
        }
      }
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  # resource defaults
  File {
    owner    => 'root',
    group    => 'root',
    mode     => '0644',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'etc_t',
    seluser  => undef,
  }
  # resources
  file { '/etc/pam.d/system-auth':
    ensure   => 'file',
    content  => template("password_quality/${::osfamily}/${::operatingsystemmajrelease}/system-auth.erb"),
  }
  file { '/etc/pam.d/password-auth':
    ensure   => 'file',
    content  => template("password_quality/${::osfamily}/${::operatingsystemmajrelease}/password-auth.erb"),
  }
  file { '/etc/security/pwquality.conf':
    ensure   => "$file_ensure",
    content  => template("password_quality/${::osfamily}/${::operatingsystemmajrelease}/pwquality.conf.erb"),
  }
}
