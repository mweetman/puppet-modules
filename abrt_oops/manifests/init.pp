# Class: abrt_oops
#
#  Manages the abrt-oops service
#
# Parameters:
#
#  [enabled]
#   Desc: Whether the abrt-oops service should be enabled
#   Args: <boolean>
#   Default: false
#
# Requires:
#
#  Service['abrtd']
#
# Sample Usage:
#
#  class { 'abrt_oops': enabled => false, }
#
class abrt_oops(
  $enabled = false,
  $package_name = 'abrt-addon-kerneloops',
  $service_name = 'abrt-oops')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    Service['abrtd'] -> Package["$package_name"]
    Package["$package_name"] -> Service["$service_name"]
  }
  else {
    $package_ensure = 'absent'
    $service_ensure = 'stopped'
    $service_enable = false
    Service["$service_name"] -> Package["$package_name"]
  }
  # resources
  package { "$package_name":
    ensure      =>  $package_ensure,
  }
  service { "$service_name":
    ensure      =>  $service_ensure,
    enable      =>  $service_enable,
    require     =>  Service['abrtd'],
  }
}
