# Class: selinux
#
#  Enables/disables selinux at boot, uses 'setenforce' for immediate changes
#
# Parameters:
#
#  [state]
#   Desc: The desired state for selinux
#   Args: enforcing, permissive, disabled
#   Default: enforcing
#
#  [type]
#   Desc: The selinux enforcement type
#   Args: targeted, strict
#   Default: targeted
#
# Sample Usage:
#
#  class { 'selinux': state => 'permissive', }
#
class selinux(
  $state = 'enforcing',
  $type  = 'targeted')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  Exec {
    path => '/bin:/usr/bin:/sbin:/usr/sbin',
  }
  file { '/etc/selinux/config':
    ensure   => 'file',
    group    => 'root',
    mode     => '0644',
    owner    => 'root',
    content  => template('selinux/config.erb'),
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'selinux_config_t',
    seluser  => undef, 
  }
  file { '/etc/sysconfig/selinux':
    ensure   => 'link',
    target   => '../selinux/config',
  }
  exec { 'setenforce_off':
    command     =>  'setenforce 0',
    onlyif      =>  "test ${state} != enforcing",
    subscribe   =>  File['/etc/selinux/config'],
    refreshonly =>  true,
  }
  exec { 'setenforce_on':
    command     =>  'setenforce 1',
    onlyif      =>  "test ${state} = enforcing",
    subscribe   =>  File['/etc/selinux/config'],
    refreshonly =>  true,
  }
}
