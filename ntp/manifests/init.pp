# Class: ntp
#
#  Manages the ntp configuration, package and service
#
# Parameters:
#
#  [enabled]
#   Desc: Determine whether the ntpd service should be installed and running
#   Args: <boolean>
#   Default: false
#
#  [servers]
#   Desc: time servers to use
#   Args: <array>
#   Default: empty
#
#  [clients]
#   Desc: clients to serve
#   Args: <hash> - network/mask pairs
#   Default: empty
#
# Sample Usage:
#
#  class { 'ntp': enabled => false, }
#
class ntp(
  $enabled = false,
  $package_name = 'ntp',
  $service_name = 'ntpd',
  $servers = [],
  $clients = {})
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    $file_ensure    = 'file'
    Package["$package_name"] -> File['/etc/ntp.conf']
    Package["$package_name"] -> File['/etc/ntp/step-tickers']
    File['/etc/ntp.conf'] ~> Service["$service_name"]
    File['/etc/ntp/step-tickers'] ~> Service["$service_name"]
  }
  else {
    $package_ensure = 'absent'
    $service_ensure = 'stopped'
    $service_enable = false
    $file_ensure    = 'absent'
    Service["$service_name"] -> File['/etc/ntp.conf']
    Service["$service_name"] -> File['/etc/ntp/step-tickers']
    File['/etc/ntp.conf'] -> Package["$package_name"]
    File['/etc/ntp/step-tickers'] -> Package["$package_name"]
  }
  # resource defaults
  File {
    owner    => 'root',
    group    => 'root',
    mode     => '0644',
    selrange => 's0',
    selrole  => 'object_r',
    seltype  => 'net_conf_t',
    seluser  =>  undef,
  }
  # resources
  package { "$package_name":
    ensure  =>  $package_ensure,
  }
  file { '/etc/ntp.conf':
    ensure   => $file_ensure,
    content  => template('ntp/ntp.conf.erb'),
  }
  file { '/etc/ntp/step-tickers':
    ensure   => $file_ensure,
    content  => template('ntp/step-tickers.erb'),
  }
  service { "$service_name":
    ensure      => $service_ensure,
    enable      => $service_enable,
    hasrestart  => true,
    hasstatus   => true,
  }
}
