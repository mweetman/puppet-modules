class { 'ntp':
         enabled => true,
         servers => ['ntp1.lab.internal','ntp2.lab.internal'],
         clients => {'10.0.0.0' => '255.255.255.0', '192.168.0.0' => '255.255.0.0'},
}
